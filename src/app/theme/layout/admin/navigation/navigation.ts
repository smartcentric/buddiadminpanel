import {Injectable} from '@angular/core';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  function?: any;
  badge?: {
    title?: string;
    type?: string;
  };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];

  
}

const NavigationItemsAdmins =[
  {
    id: 'schoollist',
    title: 'Student List',
    type: 'item',
    url: '/tbl-bootstrap/bt-basic',
    icon: 'feather icon-box'
  },
  {
    id: 'teacherlist',
    title: 'Teacher List',
    type: 'item',
    url: '/sample-page',
    icon: 'feather icon-sidebar'
  },
  {
    id: 'Payments - Student',
    title: 'Payments - Student',
    type: 'item',
    url: '/basic/alert',
    icon: 'feather icon-home'
  },
  {
    id: 'Payments - Teacher',
    title: 'Payments - Teacher',
    type: 'item',
    url: '/basic/other',
    icon: 'feather icon-home'
  },
  {
      id: 'page-layouts',
      title: 'Teachers Bank Details',
      type: 'item',
      url: '/layout/horizontal',
      icon: 'feather icon-layout'
    }
]
const NavigationItemsAdmin = [
  {
    id: 'schoollist',
    title: 'Student List',
    type: 'item',
    url: '/tbl-bootstrap/bt-basic',
    icon: 'feather icon-box'
  },
  {
    id: 'teacherlist',
    title: 'Teacher List',
    type: 'item',
    url: '/sample-page',
    icon: 'feather icon-sidebar'
  },
  {
    id: 'classes',
    title: 'Classes',
    type: 'item',
    url: '/basic/cards',
    icon: 'feather icon-file-text'
  },
  {
    id: 'courses',
    title: 'Courses',
    type: 'item',
    url: '/basic/modal',
    icon: 'feather icon-home'
  },
  {
    id: 'One on One Classes',
    title: 'One on One Classes Accepted',
    type: 'item',
    url: '/basic/progress',
    icon: 'feather icon-home'
  },
  {
    id: 'One on One Classes',
    title: 'One on One Classes Pending',
    type: 'item',
    url: '/basic/breadcrumb-paging',
    icon: 'feather icon-home'
  },
  {
    id: 'student',
    title: 'Students - Register',
    type: 'item',
    url: '/basic/tooltip-popovers',
    icon: 'feather icon-layout'
  },
   {
    id: 'school',
    title: 'School - Register',
    type: 'item',
    url: '/basic/typography',
    icon: 'feather icon-layout'
  },
   {
    id: 'contact',
    title: 'ContactUs - Register',
    type: 'item',
    url: '/basic/tabs-pills',
    icon: 'feather icon-layout'
  },
]

const NavigationItems = [
  {
    id: 'navigation',
    title: 'Navigation',
    type: 'group',
    icon: 'feather icon-monitor',
    children: [
      {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'item',
        url: '/dashboard/analytics',
        icon: 'feather icon-home'
      },
       {
        id: 'schoollist',
        title: 'Student List',
        type: 'item',
        url: '/tbl-bootstrap/bt-basic',
        icon: 'feather icon-box'
      },
      {
        id: 'teacherlist',
        title: 'Teacher List',
        type: 'item',
        url: '/sample-page',
        icon: 'feather icon-sidebar'
      },
      {
        id: 'classes',
        title: 'Classes',
        type: 'item',
        url: '/basic/cards',
        icon: 'feather icon-file-text'
      },
      {
        id: 'courses',
        title: 'Courses',
        type: 'item',
        url: '/basic/modal',
        icon: 'feather icon-home'
      },
      {
        id: 'exams',
        title: 'Exams',
        type: 'item',
         url: '/basic/spinner',
        icon: 'feather icon-home'
      },
      {
        id: 'audio',
        title: 'Audio',
        type: 'item',
        url: '/basic/collapse',
        icon: 'feather icon-home'
      },
      {
        id: 'video',
        title: 'Video',
        type: 'item',
        url: '/basic/badges',
        icon: 'feather icon-home'
      },
      {
        id: 'ebooks',
        title: 'e-Books',
        type: 'item',
        url: '/basic/carousel',
        icon: 'feather icon-home'
      },
      {
        id: 'One on One Classes',
        title: 'One on One Classes Accepted',
        type: 'item',
        url: '/basic/progress',
        icon: 'feather icon-home'
      },
      {
        id: 'One on One Classes',
        title: 'One on One Classes Pending',
        type: 'item',
        url: '/basic/breadcrumb-paging',
        icon: 'feather icon-home'
      }, {
        id: 'buddi Payments',
        title: 'buddi Payments',
        type: 'item',
        url: '/basic/alert',
        icon: 'feather icon-home'
      },
  
      {
        id: 'student',
        title: 'Students - Register',
        type: 'item',
        url: '/basic/tooltip-popovers',
        icon: 'feather icon-layout'
      },
       {
        id: 'school',
        title: 'School - Register',
        type: 'item',
        url: '/basic/typography',
        icon: 'feather icon-layout'
      },
       {
        id: 'contact',
        title: 'ContactUs - Register',
        type: 'item',
        url: '/basic/tabs-pills',
        icon: 'feather icon-layout'
      },

      {
        id: 'buddigroupclass',
        title: 'buddi Group Class',
        type: 'item',
         url: '/forms/basic',       //  this.resultData =  result.data;

        //url: '/basic/tableview',
        icon: 'feather icon-file-text'
      },
      {
        id: 'GroupDetail',
        title: 'GroupDetail',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/tableview',
        icon: 'feather icon-file-text'
      },
      {
        id: 'addcategory',
        title: 'Add Category',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/add-category',
        icon: 'feather icon-file-text'
      },
      {
        id: 'deletecategory',
        title: 'Delete Category',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/delete-category',
        icon: 'feather icon-file-text'
      },
      {
        id: 'UploadClass',
        title: 'UploadClassCourse',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/uploadclasscourse',
        icon: 'feather icon-file-text'
      },

      {
        id: 'Approvevideoclass',
        title: 'Approve Video Class',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/Approvevideoclass',
        icon: 'feather icon-file-text'
      },
      {
        id: 'Approvesmartmaterial',
        title: 'Approve Smartmaterial',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/Approvesmartmaterial',
        icon: 'feather icon-file-text'
      },
      {
        id: 'UploadStudents',
        title: 'Upload Students',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/upload-student',
        icon: 'feather icon-file-text'
      },
      {
        id: 'UploadTeachers',
        title: 'Upload Teachers',
        type: 'item',
        // url: '/forms/basic',       //  this.resultData =  result.data;

        url: '/basic/upload-teacher',
        icon: 'feather icon-file-text'
      },

      
      
      // {
      //   id: 'buddiTests',
      //   title: 'buddiTests',
      //   type: 'item',
      //   // url: '/forms/basic',       //  this.resultData =  result.data;

      //   url: '/basic/buddiTests',
      //   icon: 'feather icon-file-text'
      // },
      {
        id: 'tests',
        title: 'Buddi Test',
        type: 'group',
        icon: 'feather icon-align-left',
        children: [
          {
            id: 'tests',
            title: 'Buddi Test',
            type: 'collapse',
            icon: 'feather icon-menu',
            children: [
              {
                id: 'buddiTests',
                title: 'buddiTests',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/buddiTests',
                icon: 'feather icon-file-text'
              },
              {
                id: 'UploadTopic',
                title: 'Upload Topic',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/testuploadtopic',
                icon: 'feather icon-file-text'
              },
              {
                id: 'UploadClass',
                title: 'Upload Class',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/testuploadclass',
                icon: 'feather icon-file-text'
              },
              {
                id: 'UploadClassDocument',
                title: 'Upload Class Document',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/testuploadclassdocument',
                icon: 'feather icon-file-text'
              },
              {
                id: 'UploadVideoDocument',
                title: 'Upload Video',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/testuploadvideo',
                icon: 'feather icon-file-text'
              }
                        ]
          },
      
        ]
      },
      {
        id: 'kyclist',
        title: 'KYC List',
        type: 'group',
        icon: 'feather icon-align-left',
        children: [
          {
            id: 'kyclist',
            title: 'KYC List',
            type: 'collapse',
            icon: 'feather icon-menu',
            children: [
              {
                id: 'kyclistPending',
                title: 'kyclist Pending',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/kyclistPending',
                icon: 'feather icon-file-text'
              },
              {
                id: 'kyclistAccepted',
                title: 'kyclist Accepted',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/kyclistAccepted',
                icon: 'feather icon-file-text'
              },
              {
                id: 'kyclistRejected',
                title: 'kyclist Rejected',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/kyclistRejected',
                icon: 'feather icon-file-text'
              },
              // {
              //   id: 'menu-level-2.2',
              //   title: 'Menu Level 2.2',
              //   type: 'collapse',
              //   children: [
              //     {
              //       id: 'menu-level-2.2.1',
              //       title: 'Menu Level 2.2.1',
              //       type: 'item',
              //       url: 'javascript:',
              //       external: true
              //     },
              //     {
              //       id: 'menu-level-2.2.2',
              //       title: 'Menu Level 2.2.2',
              //       type: 'item',
              //       url: 'javascript:',
              //       external: true
              //     }
              //   ]
              // }
            ]
          },
      
        ]
      },
      {
        id: 'upilist',
        title: 'Payments',
        type: 'group',
        icon: 'feather icon-align-left',
        children: [
          {
            id: 'upilist',
            title: 'UPI',
            type: 'collapse',
            icon: 'feather icon-menu',
            children: [
              {
                id: 'upilistPending',
                title: 'Pending Transactions',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/upilistPending',
                icon: 'feather icon-file-text'
              },
              {
                id: 'upilistAccept',
                title: 'Accept Transactions',
                type: 'item',
                // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
        
                url: '/basic/upilistAccept',
                icon: 'feather icon-file-text'
              },
         
            ]
          },
          {
            id: 'razlist',
            title: 'RAZORPAY',
            type: 'collapse',
            icon: 'feather icon-menu',
            children: [     
              {
              id: 'razlistrazor',
              title: 'Razorpay Transactions',
              type: 'item',
              // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted
      
              url: '/basic/razlistrazor',
              icon: 'feather icon-file-text'
            },
          ]
        }
        ]
      },
      // {
      //   id: 'kyclistPending',
      //   title: 'kyclist Pending',
      //   type: 'item',
      //   // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted

      //   url: '/basic/kyclistPending',
      //   icon: 'feather icon-file-text'
      // },
      // {
      //   id: 'kyclistAccepted',
      //   title: 'kyclist Accepted',
      //   type: 'item',
      //   // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted

      //   url: '/basic/kyclistAccepted',
      //   icon: 'feather icon-file-text'
      // },
      // {
      //   id: 'kyclistRejected',
      //   title: 'kyclist Rejected',
      //   type: 'item',
      //   // url: '/forms/basic',       //  this.resultData =  result.data; kyclistAccepted

      //   url: '/basic/kyclistRejected',
      //   icon: 'feather icon-file-text'
      // },
      // {
      //   id: 'pages',
      //   title: 'Pages',
      //   type: 'group',
      //   icon: 'feather icon-file-text',
      //   children: [
      //     {
      //       id: 'auth',
      //       title: 'Authentication',
      //       type: 'collapse',
      //       icon: 'feather icon-lock',
      //       children: [
      //         {
      //           id: 'kyclist',
      //           title: 'kyclist',
      //           type: 'item',
      //           // url: '/forms/basic',       //  this.resultData =  result.data;
        
      //           url: '/basic/kyclist',
      //           icon: 'feather icon-file-text'
      //         },
      //         {
      //           id: 'signin',
      //           title: 'Sign in',
      //           type: 'item',
      //           url: '/auth/signin',
      //           target: true,
      //           breadcrumbs: false
      //         },
      //         {
      //           id: 'reset-password',
      //           title: 'Reset Password',
      //           type: 'item',
      //           url: '/auth/reset-password',
      //           target: true,
      //           breadcrumbs: false
      //         },
      //         {
      //           id: 'change-password',
      //           title: 'Change Password',
      //           type: 'item',
      //           url: '/auth/change-password',
      //           target: true,
      //           breadcrumbs: false
      //         }
      //       ]
      //     },
      //   ]
      // },
     
       

      // {
      //   id: 'payment',
      //   title: 'Payments',
      //   type: 'item',
      //   url: '/basic/',
      //   icon: 'feather icon-home'
      // },
   
    
   
      // {
      //   id: 'enrollment',
      //   title: 'Enrollment',
      //   type: 'item',
      //   url: '/basic/',
      //   icon: 'feather icon-sidebar'
      // },
      // {
      //   id: 'categorylists',
      //   title: 'Category Lists',
      //   type: 'item',
      //   url: '/sample-page',
      //   classes: 'nav-item',
      //   icon: 'feather icon-sidebar'
      // },
      // {
      //   id: 'page-layouts',
      //   title: 'Horizontal Layouts',
      //   type: 'item',
      //   url: '/layout/horizontal',
      //   target: false,
      //   icon: 'feather icon-layout'
      // }
    ]
  },
  // {
  //   id: 'ui-element',
  //   title: 'Categories',
  //   type: 'group',
  //   icon: 'feather icon-layers',
  //   children: [
  //     {
  //       id: 'categories',
  //       title: 'Categories',
  //       type: 'collapse',
  //       icon: 'feather icon-box',
  //       children: [
       
                // {
                //   id: 'meeting',
                //   title: 'Meeting',
                //   type: 'item',
                //   url: '/sample-page',
                //   classes: 'nav-item',
                //   icon: 'feather icon-sidebar'
                // },
                // {
                //   id: 'courses',
                //   title: 'Courses',
                //   type: 'item',
                //   url: '/basic/cards',
                //   icon: 'feather icon-home'
                // },
          // {
          //   id: 'badges',
          //   title: 'Badges',
          //   type: 'item',
          //   url: '/basic/badges'
          // },
          // {
          //   id: 'breadcrumb-pagination',
          //   title: 'Breadcrumbs & Pagination',
          //   type: 'item',
          //   url: '/basic/breadcrumb-paging'
          // },
          // {
          //   id: 'cards',
          //   title: 'Cards',
          //   type: 'item',
          //   url: '/basic/cards'
          // },
          // {
          //   id: 'collapse',
          //   title: 'Collapse',
          //   type: 'item',
          //   url: '/basic/collapse'
          // },
          // {
          //   id: 'carousel',
          //   title: 'Carousel',
          //   type: 'item',
          //   url: '/basic/carousel'
          // },
          // {
          //   id: 'grid-system',
          //   title: 'Grid System',
          //   type: 'item',
          //   url: '/basic/grid-system'
          // },
          // {
          //   id: 'progress',
          //   title: 'Progress',
          //   type: 'item',
          //   url: '/basic/progress'
          // },
          // {
          //   id: 'modal',
          //   title: 'Modal',
          //   type: 'item',
          //   url: '/basic/modal'
          // },
          // {
          //   id: 'spinner',
          //   title: 'Spinner',
          //   type: 'item',
          //   url: '/basic/spinner'
          // },
          // {
          //   id: 'tabs-pills',
          //   title: 'Tabs & Pills',
          //   type: 'item',
          //   url: '/basic/tabs-pills'
          // },
          // {
          //   id: 'typography',
          //   title: 'Typography',
          //   type: 'item',
          //   url: '/basic/typography'url: '/basic/tabs-pills'
          // },
          // {
          //   id: 'tooltip-popovers',
          //   title: 'Tooltip & Popovers',
          //   type: 'item',
          //   url: '/basic/tooltip-popovers'
          // },
          // {
          //   id: 'other',
          //   title: 'Other',
          //   type: 'item',
          //   url: '/basic/other'
          // }
  //       ]
  //     },
  //     {
  //       id: 'forms-element',
  //       title: 'Form Elements',
  //       type: 'item',
  //       url: '/forms/basic',
  //       icon: 'feather icon-file-text'
  //     }
  //   ]
  // },
  // {
  //   id: 'table',
  //   title: 'Table & Charts',
  //   type: 'group',
  //   icon: 'feather icon-list',
  //   children: [
  //     // {
  //     //   id: 'bootstrap',
  //     //   title: 'Bootstrap Table',
  //     //   type: 'item',
  //     //   url: '/tbl-bootstrap/bt-basic',
  //     //   icon: 'feather icon-server'
  //     // },
  //     {
  //       id: 'apex',
  //       title: 'Apex Chart',
  //       type: 'item',
  //       url: '/charts/apex',
  //       icon: 'feather icon-pie-chart'
  //     }
  //   ]
  // },
  // {
  //   id: 'pages',
  //   title: 'Pages',
  //   type: 'group',
  //   icon: 'feather icon-file-text',
  //   children: [
  //     {
  //       id: 'auth',
  //       title: 'Authentication',
  //       type: 'collapse',
  //       icon: 'feather icon-lock',
  //       children: [
  //         {
  //           id: 'signup',
  //           title: 'Sign up',
  //           type: 'item',
  //           url: '/auth/signup',
  //           target: true,
  //           breadcrumbs: false
  //         },
  //         {
  //           id: 'signin',
  //           title: 'Sign in',
  //           type: 'item',
  //           url: '/auth/signin',
  //           target: true,
  //           breadcrumbs: false
  //         },
  //         {
  //           id: 'reset-password',
  //           title: 'Reset Password',
  //           type: 'item',
  //           url: '/auth/reset-password',
  //           target: true,
  //           breadcrumbs: false
  //         },
  //         {
  //           id: 'change-password',
  //           title: 'Change Password',
  //           type: 'item',
  //           url: '/auth/change-password',
  //           target: true,
  //           breadcrumbs: false
  //         }
  //       ]
  //     },
  //     {
  //       id: 'maintenance',
  //       title: 'Maintenance',
  //       type: 'collapse',
  //       icon: 'feather icon-sliders',
  //       children: [
  //         {
  //           id: 'error',
  //           title: 'Error',
  //           type: 'item',
  //           url: '/maintenance/error',
  //           target: true,
  //           breadcrumbs: false
  //         },
  //         {
  //           id: 'coming-soon',
  //           title: 'Maintenance',
  //           type: 'item',
  //           url: '/maintenance/coming-soon',
  //           target: true,
  //           breadcrumbs: false
  //         }
  //       ]
  //     }
  //   ]
  // },

];

@Injectable()
export class NavigationItem {
  public get() {
    var admintype = localStorage.getItem('admintype');
    if(admintype == "1") {
      return NavigationItems;
    }
    if(admintype == "2") {
      return NavigationItemsAdmin;
    }
    if(admintype == "3") {
      return NavigationItemsAdmins;
    }
  }
}
