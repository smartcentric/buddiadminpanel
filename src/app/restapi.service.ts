import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map, catchError, tap, retry, timeout } from 'rxjs/operators';
import { Observable, of, observable, throwError, from } from 'rxjs';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';
import { environment } from '../environments/environment';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Injectable({
  providedIn: 'root'
})
export class RestapiService {

  constructor(private http: HttpClient,public toastr: ToastrManager,private router: Router) { }
  public exportJsonAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  public exportTableAsExcelFile(table: HTMLElement, excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.table_to_sheet(table);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName + new Date().getTime() + EXCEL_EXTENSION);
  }
  //public BASE_URL = 'http://52.66.110.27:4500/iBuddyPlatformAPI/';
 public BASE_URL = environment.BASE_URL;
//  private modals: any[] = [];

  private setHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Methods': 'GET,POST, PUT, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type',
    };
    return new HttpHeaders(headersConfig);
  }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
      // 'Authorization': "Bearer "+this.token
    })
  };
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    //  window.alert(errorMessage);
    if (errorMessage) {
      this.router.navigate(['/login']);
    }

    return throwError(errorMessage);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${this.BASE_URL}${path}`, { headers: this.setHeaders(), params: params });
  }

  teaccherList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getteacher', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
//   open(id: string) {
//     // open modal specified by id
//     const modal = this.modals.find(x => x.id === id);
//     modal.open();
// }
  studentList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getstudent', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  teacherpayment(token,invoicevalue): Observable<any> {
    console.log('json value', invoicevalue)
    const headers_object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return this.http.post<any>(this.BASE_URL + 'teacherAccount/getTeacherPaymentDetails',JSON.stringify(invoicevalue), headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  list(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'course/getAllCourses', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  teacherbankdetails(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'teacher/getTeacherBankDetails', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  payments(token): Observable<any> {
    console.log('json value')
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'payment/getAllTransaction', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  isFreeClass(token,formData): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
    return this.http.post<any>(this.BASE_URL + 'payment/isFreeClassAdmin',formData, headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  addfreeClass(token, formData): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
    return this.http.post<any>(this.BASE_URL + 'payment/addPaymentGetRes', formData, headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  updateClass(token, formData): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
    return this.http.post<any>(this.BASE_URL + 'payment/updateFreeClass', formData, headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  oneononeclass(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'user/alloneononeClasses', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  newapi(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'kyc/kyclist', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  Contactlist(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'contactUs/getAllContacts', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }
  paymentlist(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
    console.log(headers_object);
     return this.http.post<any>(this.BASE_URL + 'paymentDetails/getPendingUPITransactions', null, headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  Studentlist(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'studentRegister/getAllStudents', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }
  Schoollist(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'school/getAllSchools', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  exam(token): Observable<any> {
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
     return this.http.get<any>(this.BASE_URL + 'exam/getAllExams', headers_object).pipe(
    //   tap(),
    //   catchError(this.handleError <any> (''))
    // );
    retry(1),
    catchError(this.handleError)
  );

  }

  // exam(): Observable<any> {
  //   return this.http.get<any>(this.BASE_URL + 'exam/getAllExams', this.httpOptions).pipe(
  //   retry(1),
  //   catchError(this.handleError)
  // );
  // }

  classList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getclassmeeting ', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }

  getteacheruploadedvideos(token): Observable<any> {
    
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
    return this.http.get<any>(this.BASE_URL + 'teacherVideos/getVideosForVerification', headers_object).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }

  getteacheruploadedsmartmaterial(token): Observable<any> {
    
    const headers_object = {headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer ' + token})};
    return this.http.get<any>(this.BASE_URL + 'teacherSmartMaterial/getSmartMaterialForVerification', headers_object).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }


  audioList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getaudio', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
   detaillist(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getadmingroupclass', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  getsession(param: any): Observable<any> {
    console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/getsession', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
  );
  }
  gettecherid(param: any):Observable<any> {
    console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/gettecherid', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
  );
  }
   deletedetail(param: any): Observable<any> {
    console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/deletedetail', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
  );
  }
 // deletesession(param: any): Observable<any> {
  //  console.log("Param", JSON.stringify(param))
   // return this.http.post<any>(this.BASE_URL + 'user/deletesession', JSON.stringify(param), this.httpOptions).pipe(
     // retry(1),
     // catchError(this.handleError)
 // );
 // }
 getTeacherKYC(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'kyc/getTeacherKYC',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}
 uploadtestquestion(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'test/uploadTest',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

uploadtopiclist(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'classTopic/uploadTopicList',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

uploadclassdocumentlist(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'classList/uploadClassDocument',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

uploadclasslist(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'classList/uploadClassList',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

getclasslist(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'classList/getClassList',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

Updatevideostatus(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'teacherVideos/updateVideoVerificationStatus',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

Updatesmartmaterialstatus(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'teacherSmartMaterial/updateSmartMaterialVerificationStatus',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

uploadVideo(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'classList/uploadVideo',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

uploadCourseTemplate(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'classCourses/uploadCourseTemplate',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

AdminAddCategory(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'course/createCourseCategory',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}

AdminDeleteCategory(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'course/deleteCourseCategory',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}



updateKyc(data,token): Observable<any> {
  const headers_object = {
    headers: new HttpHeaders({
      'Authorization': 'Bearer ' + token
    })
  };
   return this.http.post<any>(this.BASE_URL + 'kyc/updateKyc',data,headers_object).pipe(
  retry(1),
  catchError(this.handleError)
);
}
  videoList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getvideo', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  ebookList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getebook', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  listinstructor(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'user/getinstructor', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  languageList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'course/getLanguages', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  boardList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'course/getBoards', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  gradeList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'course/getGrades', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  categoryList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'course/getCourseCategories', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  subjectList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'course/getSubjects', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }

  DocumentTypeList(): Observable<any> {
    return this.http.post<any>(this.BASE_URL + 'classDocumentType/getClassDocumentType', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  
  savedetail(param: any){
   console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/saveadmingroupclass', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
   getlistdetail(param: any){
   console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/getlistdetail', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
savesession(param: any){
   console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/savesession', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  
  editsession(param: any){
   console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/editsession', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  addmeeting (param: any) {
    console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'user/addMeetingsCategory', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }


  addcourse (param: any) {
    console.log("Param", JSON.stringify(param))
    return this.http.post<any>(this.BASE_URL + 'course/createCourseCategory', JSON.stringify(param), this.httpOptions).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }

  courseList(): Observable<any> {
    return this.http.get<any>(this.BASE_URL + 'course/getCourseCategories', this.httpOptions).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }

  deleteCategory(idValue): Observable<any> {
    // const headers_object = {headers: new HttpHeaders({
    //   'Content-Type':  'application/json'
    //   })};
     return this.http.post<any>(this.BASE_URL + 'user/deleteMeetingCategpry',JSON.stringify(idValue)).pipe(
     retry(1),
       catchError(this.handleError)
     );
    }
    deleteCourses(idValue): Observable<any> {
      // const headers_object = {headers: new HttpHeaders({
      //   'Content-Type':  'application/json'
      //   })};
       return this.http.post<any>(this.BASE_URL + 'course/deleteCourseCategory',JSON.stringify(idValue)).pipe(
       retry(1),
         catchError(this.handleError)
       );
      }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${this.BASE_URL}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    );
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${this.BASE_URL}${path}`,
      body,
      { headers: this.setHeaders() }
    );
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${this.BASE_URL}${path}`,
      { headers: this.setHeaders() }
    );
  }
  postForm(path: string, body: Object = {}): Observable<any> {
    return this.http.post(`${this.BASE_URL}${path}`, body);
  }
  getClassCourseTemplatefromModule(req): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>(this.BASE_URL + 'classCourses/getCourseTemplatesFromModule',req, headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  getClassCourseCategory(): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(this.BASE_URL + 'classCourses/getCategoriesForMenu', headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  topiclist(data,token): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      })
    };
    return this.http.post<any>(this.BASE_URL + 'classTopic/getTopicList',JSON.stringify(data), headers_object).pipe(
      retry(1),
      catchError(this.handleError)
    );
  }
  UploadStudentList(data,token): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
     return this.http.post<any>(this.BASE_URL + 'student/uploadStudentData',data,headers_object).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
  UploadTeacherList(data,token): Observable<any> {
    const headers_object = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + token
      })
    };
     return this.http.post<any>(this.BASE_URL + 'teacher/uploadTeacherData',data,headers_object).pipe(
    retry(1),
    catchError(this.handleError)
  );
  }
}
