import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { PartnerInfoComponent } from './user-info/partner-info/partner-info.component';
// import { BuddiUserComponent } from './user-info/buddi-user/buddi-user.component';
const routes: Routes = [
  {
    path: '',
    children: [
       { path: '', redirectTo: 'signin', pathMatch: 'prefix' },
      {
        path: 'analytics',
        loadChildren: () => import('./dash-analytics/dash-analytics.module').then(module => module.DashAnalyticsModule)
      },
      // {
      //   path: 'buddi-user',
      //   loadChildren: () => import('./user-info/buddi-user/buddi-user.module').then(module => module.BuddiUserModule)
      // },
      // {
      //   path: 'buddi-user',
      //   component : BuddiUserComponent
      // },
      // {
      //  path: 'partner-info',
      //   component : PartnerInfoComponent
      // }
    ]
  },
  // {
  //   path : 'buddi-user',
  //   loadChildren: () => import('./user-info/buddi-user/buddi-user.module').then(module => module.BuddiUserModule)
  // }
];



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
