import { Component, OnInit } from '@angular/core';
import { ChartDB } from '../../../fack-db/chart-data';
import { ApexChartService } from '../../../theme/shared/components/chart/apex-chart/apex-chart.service';
import { DashboardService } from '../dash-analytics/dashboard.service';
import {RestapiService} from '../../../restapi.service'
@Component({
  selector: 'app-dash-analytics',
  templateUrl: './dash-analytics.component.html',
  styleUrls: ['./dash-analytics.component.scss']
})
export class DashAnalyticsComponent implements OnInit {
  public chartDB: any;
  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  public deviceProgressBar: any;
  public userTotal: any;
  public totalPartners: any;

   
studentlength = 0;
teacherlength = 0;
videolength = 0;
ebooklength = 0;
oneacceptedlength = 0;
onependinglength = 0;
kycpendinglength =0;
kycacceptlength =0;
kycrejectlength =0;

studentregisterlength =0;
contactlength =0;
schoollistlength =0;
resultData: [];

  constructor(public apexEvent: ApexChartService, private dashboardService: DashboardService, private rest : RestapiService) {
    this.chartDB = ChartDB;
    this.dailyVisitorStatus = '1y';
    this.userTotal = 0;
    this.totalPartners = 0;
    this.deviceProgressBar = [
      {
        type: 'success',
        value: 66
      }, {
        type: 'primary',
        value: 26
      }, {
        type: 'danger',
        value: 8
      }
    ];
  }

  ngOnInit() {
    this.totalUsers();

    const webToken = localStorage.getItem('webtoken');

    this.rest.Contactlist(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('contact', result.data);
         this.contactlength = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });

       this.rest.paymentlist(webToken).subscribe((result) => {
        if (result) {
          this.resultData = result.data;
           console.log('contact', result.data);
           this.contactlength = this.resultData.length;
          } else {
           console.log('No Data');
          }
         });

this.rest.Schoollist(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('School', result.data)
         this.schoollistlength = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });

 this.rest.Studentlist(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('Student', result.data)
         this.studentregisterlength = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });

    this.rest.newapi(webToken).subscribe((result) => {
      if (result.data.accepted) {
         this.resultData =  result.data.accepted;
         this.kycacceptlength = result.data.accepted.length;
        console.log(result.data)
       
        } else {
         console.log('No Data');
        }
       });

this.rest.newapi(webToken).subscribe((result) => {
      if (result.data.rejected) {
         this.resultData =  result.data.rejected;
         this.kycrejectlength = result.data.rejected.length;
        console.log(result.data)
       
        } else {
         console.log('No Data');
        }
       });

this.rest.newapi(webToken).subscribe((result) => {
      if (result.data.pending) {
         this.resultData =  result.data.pending;
         this.kycpendinglength = result.data.pending.length;
        console.log(result.data)
       
        } else {
         console.log('No Data');
        }
       });

    console.log(webToken)
    this.rest.oneononeclass(webToken).subscribe((result) => {
      if (result) {
        //  this.resultData =  result.data;
        console.log('total oneonone class',result.data.length)
        console.log('total oneonone class',result.data)
         for(var i=0; i<result.data.length; i++){
          if (result.data[i].acceptStatus == true) {
            // this.resultData.push(result.data[i]);
            this.oneacceptedlength = this.resultData.length;
            // console.log('oneononeaccept',this.oneacceptedlength)
     
          }

      
       }
       
        } else {
         console.log('No Data');
        }
       });
       this.rest.oneononeclass(webToken).subscribe((result) => {
        if (result) {
          //  this.resultData =  result.data;
         
           for(var i=0; i<result.data.length; i++){
            if (result.data[i].acceptStatus == false) {
              // this.resultData.push(result.data[i]);
              this.onependinglength = this.resultData.length;
              // console.log('oneononeaccept',this.onependinglength)
            }
  
        
         }
         
          } else {
           console.log('No Data');
          }
         });

    this.rest.studentList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('school list', this.resultData)
         this.studentlength = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });

       this.rest.teaccherList().subscribe((result) => {
        if (result) {
           this.resultData =  result.data;
           console.log('user list', this.resultData);
           this.teacherlength = this.resultData.length;
          } else {
           console.log('No Data');
          }
         });

         this.rest.videoList().subscribe((result) => {
          if (result) {
             this.resultData =  result.data;
             console.log('video list', this.resultData)
             this.videolength = this.resultData.length;
            } else {
             console.log('No Data');
            }
           });
    
    this.rest.ebookList().subscribe((result) => {
          if (result) {
             this.resultData =  result.data;
             console.log('eBook list', this.resultData)
             this.ebooklength = this.resultData.length;
            } else {
             console.log('No Data');
            }
           });

  }

totalUsers() {
this.rest.studentList().subscribe((result) => {
  if (result) {
    this.userTotal = result.data.length;
    } else {
      this.userTotal = 0;
    }
   });

this.dashboardService.partnerCount().subscribe((result) => {
    if (result) {
      this.totalPartners = result.userCount;
      } else {
        this.totalPartners = 0;
      }
     });


 }
}
