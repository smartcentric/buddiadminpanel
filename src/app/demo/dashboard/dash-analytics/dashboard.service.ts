import { Injectable } from '@angular/core';
import { RestapiService } from '../../../restapi.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private api: RestapiService) { }
  userCount() {
    return this.api.get('user/userCount');
  }
  partnerCount() {
    return this.api.get('user/partnersCount');
  }
}
