import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../theme/shared/shared.module';
// import { BuddiUserComponent } from './user-info/buddi-user/buddi-user.component';
// import { PartnerInfoComponent } from './user-info/partner-info/partner-info.component';
import { NgbPopoverModule, NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
// import { BuddiUserRoutingModule } from './user-info/buddi-user/buddi-user-routing.module';
 // import { UserInfoRoutingModule } from './user-info/user-info-routing.module';
@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    // BuddiUserRoutingModule,
   // UserInfoRoutingModule,
    SharedModule,
    MatPaginatorModule,
    NgbProgressbarModule,
    NgbPopoverModule
  ],
  declarations: [
  //  BuddiUserComponent,
  //  PartnerInfoComponent
  ]
})
export class DashboardModule { }
