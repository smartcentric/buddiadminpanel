import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadTeacherRoutingModule } from './upload-teacher-routing.module';
import { UploadTeacherComponent } from './upload-teacher.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
  imports: [
    CommonModule,
    UploadTeacherRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [UploadTeacherComponent]
})
export class UploadTeacherModule { }
