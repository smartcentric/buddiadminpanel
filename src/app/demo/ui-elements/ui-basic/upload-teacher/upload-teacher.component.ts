import { Component, OnInit } from "@angular/core";
import { RestapiService } from "../../../../restapi.service";
import { ToastrManager } from "ng6-toastr-notifications";
import {
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Location } from "@angular/common";

@Component({
  selector: "app-upload-teacher",
  templateUrl: "./upload-teacher.component.html",
  styleUrls: ["./upload-teacher.component.scss"],
})
export class UploadTeacherComponent implements OnInit {
  testForm: FormGroup;
  submitted = false;
  formData;
  selectedFileName: string = "Choose File";

  constructor(
    private categoryservice: RestapiService,
    public location: Location,
    public toastr: ToastrManager,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.testForm = this.formBuilder.group({
      fileupload: ["", Validators.required],
      upload_name: [null],
    });
  }

  get f() {
    return this.testForm.controls;
  }

  backto() {
    this.location.back();
  }

  fileChange1(event) {
    let fileList: FileList = event.target.files;
    console.log(fileList);
    this.testForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileName = fileList[0].name;
    console.log(this.selectedFileName);
    this.testForm.patchValue({ fileupload: this.selectedFileName });
    // this.testForm.get('fileupload').setValue(file);
  }

  onSubmit() {
    var btn = document.getElementById("addCategoryButton") as HTMLButtonElement;
    btn.disabled = true;
    this.submitted = true;
    let data = this.testForm.value;
    console.log(data)
    if (!this.testForm.valid) {
      const invalid = [];
      const controls = this.testForm.controls;
      btn.disabled = false;
      for (const name in controls) {
        if (controls[name].invalid) {
          if (name == "fileupload") {
            this.toastr.warningToastr("Excel required");
          } else {
            this.toastr.warningToastr("Invalid Fields");
          }
        }
        return;
      }
    }

     this.formData = new FormData();
     this.formData.append("excel_file", data.upload_name, data.upload_name.name);
    var token = localStorage.getItem("webtoken");

    this.categoryservice.UploadTeacherList(this.formData, token).subscribe((response) => {
      console.log(response);
      if (response.message == "Inserted Successfully") {
        this.toastr.successToastr(response.message);
        this.backto();
      } else {
        btn.disabled = false;
        this.toastr.errorToastr(response.message);
      }
    });
  }
}
