import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadTeacherComponent } from './upload-teacher.component';

const routes: Routes = [{ path: '', component: UploadTeacherComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadTeacherRoutingModule { }
