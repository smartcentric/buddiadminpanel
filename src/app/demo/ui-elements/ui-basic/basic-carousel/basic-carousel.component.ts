import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-basic-carousel',
  templateUrl: './basic-carousel.component.html',
  styleUrls: ['./basic-carousel.component.scss']
})
export class BasicCarouselComponent implements OnInit {
  fileName= 'e-BooksList.xlsx';
  resultData: [];
  
  term: string;
  profilename : any;
  p: number =1;
  // categoryname : [];
  // gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];
  length = 0;
  firstName : [];
  // firstName : [];
  lastName : [];
  profileName : [];
  schoolName : [];
  email : [];
  phoneNumber : [];
  teacherID : [];
  groupName : [];
  address1 : [];
  address2 : [];
  area : [];
  city : [];
  country : [];
  showModal : boolean;
  constructor(public toastr: ToastrManager,private categoryservice: RestapiService) {

  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'e-BooksList');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
    onClick(event,user)
    {
      console.log('haimodal',user)
      this.showModal = true; // Show-Hide Modal Check
      this.firstName = user.teacherID.firstName;
      this.lastName = user.teacherID.lastName;
      this.profileName = user.teacherID.profileName;
      this.schoolName = user.teacherID.schoolName;
      this.email = user.teacherID.email;
      this.phoneNumber = user.teacherID.phoneNumber;
      this.teacherID = user.teacherID.teacherID;
      this.groupName = user.teacherID.groupName;
      this.address1 = user.teacherID.address1;
      this.address2 = user.teacherID.address2;
      this.area = user.teacherID.area;
      this.city = user.teacherID.city;
      this.country = user.teacherID.country;
      
      console.log('haianothermodal',user)
       
    }
    //Bootstrap Modal Close event
    hide()
    {
      this.showModal = false;
    }

  ngOnInit() {
    this.listeBook();
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
    listeBook() {
    this.categoryservice.ebookList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('eBook list', this.resultData)
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
  }

}
