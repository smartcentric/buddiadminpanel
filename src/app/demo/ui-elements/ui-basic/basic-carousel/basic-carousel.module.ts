import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicCarouselRoutingModule } from './basic-carousel-routing.module';
import { BasicCarouselComponent } from './basic-carousel.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbCarouselModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicCarouselRoutingModule,
    SharedModule,
    NgbCarouselModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ],
  declarations: [BasicCarouselComponent]
})
export class BasicCarouselModule { }
