import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TeacherClassVideoApproveComponent} from './teacher-class-video-approve.component'

const routes: Routes = [
  {
    path: '',
    component: TeacherClassVideoApproveComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherClassVideoApproveRoutingModule { }
