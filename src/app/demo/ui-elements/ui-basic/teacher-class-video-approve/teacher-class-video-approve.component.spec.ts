import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherClassVideoApproveComponent } from './teacher-class-video-approve.component';

describe('TeacherClassVideoApproveComponent', () => {
  let component: TeacherClassVideoApproveComponent;
  let fixture: ComponentFixture<TeacherClassVideoApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherClassVideoApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherClassVideoApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
