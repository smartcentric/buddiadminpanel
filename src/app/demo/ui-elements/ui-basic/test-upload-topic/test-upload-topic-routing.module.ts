import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestUploadTopicComponent} from './test-upload-topic.component';

const routes: Routes = [
  {
    path: '',
    component: TestUploadTopicComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestUploadTopicRoutingModule { }