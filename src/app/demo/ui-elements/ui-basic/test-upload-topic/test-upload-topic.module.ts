import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestUploadTopicRoutingModule } from './test-upload-topic-routing.module';
import { TestUploadTopicComponent } from './test-upload-topic.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    TestUploadTopicRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [TestUploadTopicComponent]
})
export class TestUploadTopicModule { }
