import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-test-upload-topic',
  templateUrl: './test-upload-topic.component.html',
  styleUrls: ['./test-upload-topic.component.scss']
})
export class TestUploadTopicComponent implements OnInit { 
  testForm: FormGroup;
  submitted = false;
  languageapi: any =[];
  categoryapi: any = [];
  gradeapi: any = [];
  boardapi: any = [];
  subjectapi: any = [];
  public paramData:any = [];  
  selectedFileName: string = 'Choose File';
  selectedFileName1: string = 'Choose File';
  public subjectarray: any = [];
  formData;  
  dropdownSettings: any = {};
  dropdowngradeSettings: any = {};
  dropdownsubjectSettings: any = {};
  listsubjects : any;
  selectedsubjectlist = [];



  constructor(private categoryservice: RestapiService , public location: Location,public toastr: ToastrManager,private formBuilder: FormBuilder) { }

  ngOnInit(): void {


    this.listCategory();
    this.listboard();
    this.listlanguage();
    this.listgrade();
    this.listsubject();

    this.dropdownsubjectSettings = {
      singleSelection: false,
      idField: 'subjectName',
      textField: 'subjectName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3
    };

    this.testForm = this.formBuilder.group({
                  fileupload: ['', Validators.required],
                  category: ['', Validators.required],
                  subject: [''],
                  board: [''],
                  grade:[''],
                  language:[''],
                  upload_name: [null],
      })
  }

  get f() { return this.testForm.controls; }
   backto(){
    this.location.back();
  }

  listCategory() {
    this.categoryservice.categoryList().subscribe((result) => {
      if (result) {
         this.categoryapi =  result.data;
         
         if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ category: this.categoryapi[0]._id })
      }
      console.log(this.categoryapi);
      }
        else {
         console.log('No Data');
        }
       });
  }

  listboard() {
    this.categoryservice.boardList().subscribe((result) => {
      if (result) {
         this.boardapi =  result.data;
          if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ board: this.boardapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }


  listgrade() {
    this.categoryservice.gradeList().subscribe((result) => {
      if (result) {
         this.gradeapi =  result.data;
         if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ grade: this.gradeapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  listsubject() {
    this.categoryservice.subjectList().subscribe((result) => {
      if (result) {
         this.listsubjects = result.data;
         this.subjectapi =  result.data;
         if(Object.keys(this.paramData).length == 0){
       this.testForm.patchValue({ subject: this.subjectapi[0]._id })
     }
        } else {
         console.log('No Data');
        }
       });
  }

  listlanguage() {
    this.categoryservice.languageList().subscribe((result) => {
      if (result) {
         this.languageapi =  result.data;
            if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ language: this.languageapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  fileChange1(event) {
    let fileList: FileList = event.target.files;
   // const file = event.target.files[0];
   // console.log(fileList);
    this.testForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileName = fileList[0].name;
    this.testForm.patchValue({ fileupload: this.selectedFileName });
   // this.testForm.get('fileupload').setValue(file);
  }

  onSubmit() {
    var btn = (document.getElementById('submitButton') as HTMLButtonElement);
    btn.disabled = true;
    const webToken = localStorage.getItem('webtoken');
    this.submitted = true;
     // stop here if form is invalid
     if (this.testForm.invalid) {
      btn.disabled = false;
      this.toastr.errorToastr("Invalid Fields");
        return;
    }     
    const data = this.testForm.value; 
    if(data.upload_name!=null) {
    //  const startDate1 = new Date(this.testForm.value.startdate).getTime() / 1000    
      this.formData = new FormData();
      this.formData.append("excel_file", data.upload_name, data.upload_name.name);
      this.formData.append("category", data.category);
      this.formData.append("grade", data.grade);
      this.formData.append("board", data.board);
      this.formData.append("subject", data.subject);
      this.formData.append("language", data.language);


    console.log(this.formData);
      this.categoryservice.uploadtopiclist(this.formData, webToken).subscribe((result) => {
        if(result.status) {
          this.toastr.successToastr(result.message);
          this.backto()
        }
        else {
          btn.disabled = false;
          
          this.toastr.errorToastr(result.message);
        }

      }, (err) => {
        btn.disabled = false;
    console.log(err);
    });

    }   
    else {
      btn.disabled = false;
      this.toastr.errorToastr("please upload excel file");
    }


  }

  onItemsubjectSelect(item: any) {
    this.selectedsubjectlist.push(item._id)
   }
   
   onItemsubjectDeSelect(item1: any) {
     this.selectedsubjectlist = this.selectedsubjectlist.filter(item => !item1._id.includes(item))
   }
  
   onItemsubjectallDeSelect(item1: any) {
    for (let i = 0; i < item1.length; i++) {
      this.selectedsubjectlist = this.selectedsubjectlist.filter(item => !item1[i]._id.includes(item))
      };
  }
  onItemsubjectallSelect(item1: any) {
    this.selectedsubjectlist = [];
    for (let i = 0; i < item1.length; i++) {
      this.selectedsubjectlist.push(item1[i]._id)
      };
  }

}
