import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestUploadTopicComponent } from './test-upload-topic.component';

describe('TestUploadTopicComponent', () => {
  let component: TestUploadTopicComponent;
  let fixture: ComponentFixture<TestUploadTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestUploadTopicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestUploadTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
