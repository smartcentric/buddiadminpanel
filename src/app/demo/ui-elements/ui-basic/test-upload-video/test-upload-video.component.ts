import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-test-upload-video',
  templateUrl: './test-upload-video.component.html',
  styleUrls: ['./test-upload-video.component.scss']
})
export class TestUploadVideoComponent implements OnInit { 
  testForm: FormGroup;
  videoForm: FormGroup;
  submitted = false;
  languageapi: any =[];
  searchresultapi: any = [];
  categoryapi: any = [];
  gradeapi: any = [];
  boardapi: any = [];
  subjectapi: any = [];
  public paramData:any = [];  
  selectedFileName: string = 'Choose File';
  selectedFileName2: string = 'Choose File';
  selectedFileName3: string = 'Choose File';
  public subjectarray: any = [];
  formData;  
  dropdownSettings: any = {};
  dropdowngradeSettings: any = {};
  dropdownsubjectSettings: any = {};
  listsubjects : any;
  selectedsubjectlist = [];
  powerpointList: File[] = [];
  thumbnailList: File[] = [];
  public searchclass: boolean = false;
  public uploadvideo: boolean = false;
  filterclasspath : any;



  constructor(private categoryservice: RestapiService , public location: Location,public toastr: ToastrManager,private formBuilder: FormBuilder) { }

  ngOnInit(): void {


    this.listCategory();
    this.listboard();
    this.listlanguage();
    this.listgrade();
    this.listsubject();

    this.dropdownsubjectSettings = {
      singleSelection: false,
      idField: 'subjectName',
      textField: 'subjectName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3
    };

    this.testForm = this.formBuilder.group({
                  category: ['', Validators.required],
                  subject: [''],
                  board: [''],
                  grade:[''],
                  language:['']                 
      })

      this.videoForm = this.formBuilder.group({
        fileupload: ['', Validators.required],
        capsuleid: [''],    
        upload_name: [null],  
        level:['']
})

this.searchclass = true;
  }

  get f() { return this.testForm.controls; }

  get s() { return this.videoForm.controls; }
   backto(){
    this.location.back();
  }

  listCategory() {
    this.categoryservice.categoryList().subscribe((result) => {
      if (result) {
         this.categoryapi =  result.data;
         
         if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ category: this.categoryapi[0]._id })
      }
      console.log(this.categoryapi);
      }
        else {
         console.log('No Data');
        }
       });
  }

  listboard() {
    this.categoryservice.boardList().subscribe((result) => {
      if (result) {
         this.boardapi =  result.data;
          if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ board: this.boardapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }


  listgrade() {
    this.categoryservice.gradeList().subscribe((result) => {
      if (result) {
         this.gradeapi =  result.data;
         if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ grade: this.gradeapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  listsubject() {
    this.categoryservice.subjectList().subscribe((result) => {
      if (result) {
         this.listsubjects = result.data;
         this.subjectapi =  result.data;
         if(Object.keys(this.paramData).length == 0){
       this.testForm.patchValue({ subject: this.subjectapi[0]._id })
     }
        } else {
         console.log('No Data');
        }
       });
  }


  listlanguage() {
    this.categoryservice.languageList().subscribe((result) => {
      if (result) {
         this.languageapi =  result.data;
            if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ language: this.languageapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  fileChange1(event) {
    let fileList: FileList = event.target.files;
   // const file = event.target.files[0];
   // console.log(fileList);
   console.log(fileList[0]);
    this.videoForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileName = fileList[0].name;
    this.videoForm.patchValue({ fileupload: this.selectedFileName });
   // this.testForm.get('fileupload').setValue(file);
  }


  onSubmit() {
    var btn = (document.getElementById('submitButton') as HTMLButtonElement);
    btn.disabled = true;
    const webToken = localStorage.getItem('webtoken');
    this.submitted = true;
     // stop here if form is invalid
     if (this.videoForm.invalid) {
      // const invalid = [];
      // const controls = this.testForm.controls;
      // for (const name in controls) {
      //   if (controls[name].invalid) {
      //     alert(name);
      //   }
      // }
      btn.disabled = false;
      this.toastr.errorToastr("Invalid Fields");
        return;
    }     
    const data = this.videoForm.value; 
    if(data.upload_name!=null) {
    //  const startDate1 = new Date(this.testForm.value.startdate).getTime() / 1000    
      this.formData = new FormData();
      this.formData.append("video_file", data.upload_name);
      this.formData.append("capsuleID", data.capsuleid);      
      this.formData.append("videoName", data.upload_name.name);   
      this.formData.append("level", data.level);   
      // this.filterclasspath = this.searchresultapi.find(item => item._id == data.capsuleid);
      // this.formData.append("classPath", this.filterclasspath.s3Path);  
      for (var pair of this.formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    } 
      this.categoryservice.uploadVideo(this.formData, webToken).subscribe((result) => {
        if(result.status) {
          this.toastr.successToastr(result.message);
          this.backto()
        }
        else {
          btn.disabled = false;
          this.toastr.errorToastr(result.message);
        }

      }, (err) => {
        btn.disabled = false;
    console.log(err);
    });

    }   
    else {
      btn.disabled = false;
      this.toastr.errorToastr("please upload video file");
    }


  }

  
  onSearch() {
    const webToken = localStorage.getItem('webtoken');
    this.submitted = true;
     // stop here if form is invalid
     if (this.testForm.invalid) {
      // const invalid = [];
      // const controls = this.testForm.controls;
      // for (const name in controls) {
      //   if (controls[name].invalid) {
      //     alert(name);
      //   }
      // }
      this.toastr.errorToastr("Invalid Fields");
        return;
    }     
    const data = this.testForm.value; 
    var req={
      categoryID:data.category,
      gradeID:data.grade,
      boardID:data.board,
      subjectID:data.subject,
      languageID: data.language      
    }
    console.log(req);
      this.categoryservice.getclasslist(req, webToken).subscribe((result) => {
        if(result.status) {
          this.searchclass = false;
          this.uploadvideo = true;
          this.searchresultapi =  result.data;
          if(Object.keys(this.paramData).length == 0){
        this.videoForm.patchValue({ capsuleid: this.searchresultapi[0]._id })
        }
      }
        else {
          this.toastr.errorToastr(result.message);
        }

      }, (err) => {
    console.log(err);
    });

  }


  onItemsubjectSelect(item: any) {
    this.selectedsubjectlist.push(item._id)
   }
   
   onItemsubjectDeSelect(item1: any) {
     this.selectedsubjectlist = this.selectedsubjectlist.filter(item => !item1._id.includes(item))
   }
  
   onItemsubjectallDeSelect(item1: any) {
    for (let i = 0; i < item1.length; i++) {
      this.selectedsubjectlist = this.selectedsubjectlist.filter(item => !item1[i]._id.includes(item))
      };
  }
  onItemsubjectallSelect(item1: any) {
    this.selectedsubjectlist = [];
    for (let i = 0; i < item1.length; i++) {
      this.selectedsubjectlist.push(item1[i]._id)
      };
  }

}
