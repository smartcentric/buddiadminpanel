import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestUploadVideoRoutingModule } from './test-upload-video-routing.module';
import { TestUploadVideoComponent } from './test-upload-video.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    TestUploadVideoRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [TestUploadVideoComponent]
})
export class TestUploadVideoModule { }
