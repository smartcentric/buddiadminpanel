import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestUploadVideoComponent} from './test-upload-video.component';

const routes: Routes = [
  {
    path: '',
    component: TestUploadVideoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestUploadVideoRoutingModule { }