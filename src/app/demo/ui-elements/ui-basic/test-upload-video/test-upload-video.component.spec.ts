import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestUploadVideoComponent } from './test-upload-video.component';

describe('TestUploadVideoComponent', () => {
  let component: TestUploadVideoComponent;
  let fixture: ComponentFixture<TestUploadVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestUploadVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestUploadVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
