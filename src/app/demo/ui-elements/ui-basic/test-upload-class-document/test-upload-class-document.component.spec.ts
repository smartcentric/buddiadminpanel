import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestUploadClassDocumentComponent } from './test-upload-class-document.component';

describe('TestUploadClassDocumentComponent', () => {
  let component: TestUploadClassDocumentComponent;
  let fixture: ComponentFixture<TestUploadClassDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestUploadClassDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestUploadClassDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
