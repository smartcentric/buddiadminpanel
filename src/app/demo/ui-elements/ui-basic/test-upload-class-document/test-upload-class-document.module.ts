import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestUploadClassDocumentRoutingModule } from './test-upload-class-document-routing.module';
import { TestUploadClassDocumentComponent } from './test-upload-class-document.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    TestUploadClassDocumentRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [TestUploadClassDocumentComponent]
})
export class TestUploadClassDocumentModule { }
