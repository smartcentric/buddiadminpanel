import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestUploadClassDocumentComponent} from './test-upload-class-document.component';

const routes: Routes = [
  {
    path: '',
    component: TestUploadClassDocumentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestUploadClassDocumentRoutingModule { }