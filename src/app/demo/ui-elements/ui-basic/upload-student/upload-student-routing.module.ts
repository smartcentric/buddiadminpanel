import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadStudentComponent } from './upload-student.component';

const routes: Routes = [{ path: '', component: UploadStudentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadStudentRoutingModule { }
