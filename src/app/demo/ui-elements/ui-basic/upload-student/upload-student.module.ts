import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadStudentRoutingModule } from './upload-student-routing.module';
import { UploadStudentComponent } from './upload-student.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
  imports: [
    CommonModule,
    UploadStudentRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [UploadStudentComponent]
})
export class UploadStudentModule { }
