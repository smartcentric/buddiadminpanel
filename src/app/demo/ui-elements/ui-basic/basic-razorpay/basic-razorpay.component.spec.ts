import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicRazorpayComponent } from './basic-razorpay.component';

describe('BasicRazorpayComponent', () => {
  let component: BasicRazorpayComponent;
  let fixture: ComponentFixture<BasicRazorpayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicRazorpayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicRazorpayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
