import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicRazorpayRoutingModule } from './basic-razorpay-routing.module';
import { BasicRazorpayComponent } from './basic-razorpay.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicRazorpayRoutingModule,
    SharedModule,
    NgbProgressbarModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ],
  declarations: [BasicRazorpayComponent]
})
export class BasicRazorpayModule { }