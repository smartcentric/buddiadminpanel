import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasicRazorpayComponent } from './basic-razorpay.component';

const routes: Routes = [
  {
    path: '',
    component: BasicRazorpayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicRazorpayRoutingModule { }