import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-basic-collapse',
  templateUrl: './basic-detail.component.html',
  styleUrls: ['./basic-detail.component.scss']
})
export class BasicDetailComponent implements OnInit {
  public isCollapsed: boolean;
  public multiCollapsed1: boolean;
  public multiCollapsed2: boolean;

  resultData: [];
  term: string;
  p: number =1;
  profilename : any;
  list : [];
  length = 0;
  category : [];
  categoryname : [];
  gradename : [];
  boardname : [];
  subjectname : [];
  languagename : [];

  fileName= 'GroupDetails.xlsx';
  constructor(public toastr: ToastrManager,private categoryservice: RestapiService,private router: Router) {

  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'GroupDetails');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
  ngOnInit() {
    this.isCollapsed = true;
    this.multiCollapsed1 = true;
    this.multiCollapsed2 = true;

    this.listDetail();
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

goto(id){
  console.log("ingoto")

  this.router.navigate(['/basic/sessiondetail/:'+id]);
}
 delete(id){
  var rst={
id:id,

  }
   this.categoryservice.deletedetail(rst).subscribe((result) => {
if (result) {
  console.log(result);
  this.toastr.successToastr('Hooray! Your data has been removed');
  this.listDetail();
}

   });
 }
  listDetail() {
    this.categoryservice.detaillist().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         this.length = this.resultData.length;
         console.log('audio list', this.resultData);
        for(var i=0; i<result.data.length; i++){
          this.categoryname = result.data[i].categoryID.categoryName;
          this.gradename = result.data[i].gradeID.gradeName;
          this.boardname = result.data[i].boardID.boardName;
          this.languagename = result.data[i].languageID.languageName;
          this.subjectname = result.data[i].subjectID.subjectName;
          // console.log('category audio',result.data[i].categoryID.categoryName)
        }
        } else {
         console.log('No Data');
        }
       });
  }
}
