import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UpiPendingComponent} from './upi-pending.component';

const routes: Routes = [
  {
    path: '',
    component: UpiPendingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpiPendingRoutingModule { }