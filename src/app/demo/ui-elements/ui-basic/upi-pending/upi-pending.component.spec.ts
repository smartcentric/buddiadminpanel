import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpiPendingComponent } from './upi-pending.component';

describe('UpiPendingComponent', () => {
  let component: UpiPendingComponent;
  let fixture: ComponentFixture<UpiPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpiPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpiPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
