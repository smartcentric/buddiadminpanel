import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiBasicRoutingModule } from './ui-basic-routing.module';
import { FormElementsRoutingModule } from '../../pages/form-elements/form-elements-routing.module';
import { KyclistComponent } from './kyclist/kyclist.component';
// import { KyclistComponent } from './kyclist/kyclist.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { KycAcceptedComponent } from './kyc-accepted/kyc-accepted.component';
import { KycRejectedComponent } from './kyc-rejected/kyc-rejected.component';
import { TeacherPaymentComponent } from './teacher-payment/teacher-payment.component';
import { UpiPendingComponent } from './upi-pending/upi-pending.component';
import { UpiAcceptComponent } from './upi-accept/upi-accept.component';
import { BasicRazorpayComponent } from './basic-razorpay/basic-razorpay.component';

@NgModule({
  imports: [
    CommonModule,
    UiBasicRoutingModule,
    FormElementsRoutingModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [KyclistComponent, KycAcceptedComponent, KycRejectedComponent, TeacherPaymentComponent, UpiPendingComponent, UpiAcceptComponent, BasicRazorpayComponent]
})
export class UiBasicModule { }
