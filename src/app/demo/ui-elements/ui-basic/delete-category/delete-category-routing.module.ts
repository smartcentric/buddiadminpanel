import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeleteCategoryComponent } from './delete-category.component';

const routes: Routes = [{ path: '', component: DeleteCategoryComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeleteCategoryRoutingModule { }
