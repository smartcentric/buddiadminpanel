import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeleteCategoryRoutingModule } from './delete-category-routing.module';
import { DeleteCategoryComponent } from './delete-category.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
  imports: [
    CommonModule,
    DeleteCategoryRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [DeleteCategoryComponent]
})
export class DeleteCategoryModule { }
