import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-delete-category',
  templateUrl: './delete-category.component.html',
  styleUrls: ['./delete-category.component.scss']
})
export class DeleteCategoryComponent implements OnInit {

  resultData: [];
  term: string;
  p: number =1;
  profilename : any;
  // categoryname : [];
  // gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];

  firstName : [];
  // firstName : [];
  lastName : [];
  profileName : [];
  schoolName : [];
  email : [];
  phoneNumber : [];
  teacherID : [];
  groupName : [];
  address1 : [];
  address2 : [];
  area : [];
  city : [];
  country : [];
  showModal : boolean;
  length = 0;
  registerForm: FormGroup;
  selectedFileNmae: string = 'Upload Photo';
  fileName= 'ClassesList.xlsx';
  
  constructor(public toastr: ToastrManager,private categoryservice: RestapiService,private formBuilder: FormBuilder) { }
  
  
  onClick(event,user)
  {
    console.log('haimodal',user)
    this.showModal = true; // Show-Hide Modal Check
    this.firstName = user.teacherID.firstName;
    this.lastName = user.teacherID.lastName;
    this.profileName = user.teacherID.profileName;
    this.schoolName = user.teacherID.schoolName;
    this.email = user.teacherID.email;
    this.phoneNumber = user.teacherID.phoneNumber;
    this.teacherID = user.teacherID.teacherID;
    this.groupName = user.teacherID.groupName;
    this.address1 = user.teacherID.address1;
    this.address2 = user.teacherID.address2;
    this.area = user.teacherID.area;
    this.city = user.teacherID.city;
    this.country = user.teacherID.country;
    
    console.log('haianothermodal',user)
     
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }
  
  ngOnInit() {

    this.listCourseCategory();
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken)

  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  get f() { return this.registerForm.controls; }

  listCourseCategory() {
    this.categoryservice.categoryList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('class list', this.resultData)
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
  }

  deleteCategory(data){
    if(confirm("Are you sure want to delete this category?")) {
      //**Passing webtoken and lifeid to backend */
      var webToken = localStorage.getItem('webtoken');
      var reqData = {};
      reqData['id'] = data._id;
      this.categoryservice.AdminDeleteCategory(reqData, webToken).subscribe((result) => {
        console.log(result)
       if (result.status) {
          this.toastr.successToastr('Category deleted','Success',{
             position:'top-right'
          });
          this.ngOnInit();
        }
        else{
          console.log(result.message);
        this.toastr.errorToastr("Error on deleting")
        }
       }, (err) => {
       console.log(err);
       this.toastr.errorToastr("Error on deleting")
       });
    }
   }

}
