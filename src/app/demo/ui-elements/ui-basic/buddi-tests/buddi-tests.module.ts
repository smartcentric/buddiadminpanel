import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuddiTestsRoutingModule } from './buddi-tests-routing.module';
import { BuddiTestsComponent } from './buddi-tests.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BuddiTestsRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [BuddiTestsComponent]
})
export class BuddiTestsModule { }
