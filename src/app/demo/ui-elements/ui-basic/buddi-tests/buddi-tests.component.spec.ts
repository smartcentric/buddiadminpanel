import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuddiTestsComponent } from './buddi-tests.component';

describe('BuddiTestsComponent', () => {
  let component: BuddiTestsComponent;
  let fixture: ComponentFixture<BuddiTestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuddiTestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuddiTestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
