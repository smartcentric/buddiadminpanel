import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-buddi-tests',
  templateUrl: './buddi-tests.component.html',
  styleUrls: ['./buddi-tests.component.scss']
})
export class BuddiTestsComponent implements OnInit { 
  testForm: FormGroup;


  submitted = false;
  languageapi: any =[];
  categoryapi: any = [];
  gradeapi: any = [];
  boardapi: any = [];
  subjectapi: any = [];
  public paramData:any = [];  
  selectedFileName: string = 'Choose File';
  selectedFileName1: string = 'Choose File';
  public subjectarray: any = [];
  formData;  
  dropdownSettings: any = {};
  dropdowngradeSettings: any = {};
  dropdownsubjectSettings: any = {};
  listsubjects : any;
  selectedsubjectlist = [];
  categoryselectedValue = "";
  boardselectedValue = "";
  languageselectedValue = "";
  gradeselectedValue = "";
  subjectselectedValue = "";
  moduleselectedValue = "";
  templateselectedValue = "";
  materialidselectedValue = "";
  topicselectedValue = "";
  public topicapi: any = [];
  public coursecategorylist: any = [];
  public modulelist: any = [];
  public templatelist: any = [];
  isfromcoursedetails: any;

  constructor(private categoryservice: RestapiService , public location: Location,public toastr: ToastrManager,private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.listlanguage();
    this.getcoursecategory();
    this.dropdownsubjectSettings = {
      singleSelection: false,
      idField: 'subjectName',
      textField: 'subjectName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3
    };

    this.testForm = this.formBuilder.group({
                  testname: ['', Validators.required],
                  testseries: ['', Validators.required],
                  sectionformat: ['', Validators.required],
                  fileupload: ['', Validators.required],
                  pdfupload: ['', Validators.required],
                  numberofquestions: ['', Validators.required],
                  totaltime: ['', Validators.required],
                  totalmarks: ['', Validators.required],
                  startdate:['',Validators.required],
                  courseCategory: ['', Validators.required],
                  courseModule: ['',  Validators.required],
                  courseTemplate:['',  Validators.required],
                  language:[''],
                  upload_name: [null],
                  pdfupload_name: [null]
      })
  }

  get f() { return this.testForm.controls; }
   backto(){
    this.location.back();
  }

  listCategory() {
    this.categoryservice.categoryList().subscribe((result) => {
      if (result) {
         this.categoryapi =  result.data;
         
         if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ category: this.categoryapi[0].categoryName })
      }
      console.log(this.categoryapi);
      }
        else {
         console.log('No Data');
        }
       });
  }

  listboard() {
    this.categoryservice.boardList().subscribe((result) => {
      if (result) {
         this.boardapi =  result.data;
         console.log('user list', this.boardapi);
          if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ board: this.boardapi[0].boardName })
      }
        } else {
         console.log('No Data');
        }
       });
  }


  listgrade() {
    this.categoryservice.gradeList().subscribe((result) => {
      if (result) {
         this.gradeapi =  result.data;
         console.log('user list', this.gradeapi);
         if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ grade: this.gradeapi[0].gradeName })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  listsubject() {
    this.categoryservice.subjectList().subscribe((result) => {
      if (result) {
         this.listsubjects = result.data;
         this.subjectapi =  result.data;
         console.log('user list', this.subjectapi);
         if(Object.keys(this.paramData).length == 0){
       this.testForm.patchValue({ subject: this.subjectapi[0].subjectName })
     }
        } else {
         console.log('No Data');
        }
       });
  }

  listlanguage() {
    this.categoryservice.languageList().subscribe((result) => {
      if (result) {
         this.languageapi =  result.data;
         console.log('user list', this.languageapi);
            if(Object.keys(this.paramData).length == 0){
        this.testForm.patchValue({ language: this.languageapi[0].languageName })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  fileChange1(event) {
    let fileList: FileList = event.target.files;
   // const file = event.target.files[0];
   // console.log(fileList);
    this.testForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileName = fileList[0].name;
    console.log(this.selectedFileName);
    this.testForm.patchValue({ fileupload: this.selectedFileName });
   // this.testForm.get('fileupload').setValue(file);
  }

  fileChange2(event) {
    let fileList: FileList = event.target.files;
   // const file = event.target.files[0];
   // console.log(fileList);
    this.testForm.patchValue({ pdfupload_name: fileList[0] });
    this.selectedFileName1 = fileList[0].name;
    console.log(this.selectedFileName1);
    this.testForm.patchValue({ pdfupload: this.selectedFileName1 });
   // this.testForm.get('fileupload').setValue(file);
  }

  onSubmit() {
    const webToken = localStorage.getItem('webtoken');
    this.submitted = true;
     // stop here if form is invalid
     if (this.testForm.invalid) {
       console.log(this.testForm.controls)
      console.log("Invalid Fields");
      this.toastr.errorToastr("Invalid Fields");
        return;
    }     
    const data = this.testForm.value; 
    if(data.upload_name!=null) {
      this.subjectarray = [];
      this.subjectarray.push(data.subject);
      const startDate1 = new Date(this.testForm.value.startdate).getTime() / 1000    
      this.formData = new FormData();
      this.formData.append("excel_file", data.upload_name, data.upload_name.name);
      this.formData.append("test_file", data.pdfupload_name, data.pdfupload_name.name);
      this.formData.append("testName", data.testname);
      this.formData.append("sectionFormat", data.sectionformat);
      this.formData.append("testSeriesType", data.testseries);
      this.formData.append("courseID", data.courseTemplate);
      this.formData.append("moduleID", data.courseModule);
      this.formData.append("categoryID", data.courseCategory);
      this.formData.append("subject", data.subject);
      this.formData.append("language", data.language);
      this.formData.append("numberOfQuestions", data.numberofquestions);
      this.formData.append("totalTime", data.totaltime);
      this.formData.append("totalMarks", data.totalmarks);
      this.formData.append("scheduledDate", new Date(this.testForm.value.startdate).getTime());

      for (var pair of this.formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }

    console.log(this.formData);
      this.categoryservice.uploadtestquestion(this.formData, webToken).subscribe((result) => {
        if(result.status) {
          this.toastr.successToastr(result.message);
          this.backto()
        }
        else {
          this.toastr.errorToastr(result.message);
        }

      }, (err) => {
    console.log(err);
    });

    }   
    else {
      this.toastr.errorToastr("please upload excel file");
    }


  }

  onItemsubjectSelect(item: any) {
    this.selectedsubjectlist.push(item._id)
   }
   
   onItemsubjectDeSelect(item1: any) {
     this.selectedsubjectlist = this.selectedsubjectlist.filter(item => !item1._id.includes(item))
   }
  
   onItemsubjectallDeSelect(item1: any) {
    for (let i = 0; i < item1.length; i++) {
      this.selectedsubjectlist = this.selectedsubjectlist.filter(item => !item1[i]._id.includes(item))
      };
  }
  onItemsubjectallSelect(item1: any) {
    this.selectedsubjectlist = [];
    for (let i = 0; i < item1.length; i++) {
      this.selectedsubjectlist.push(item1[i]._id)
      };
  }

  selectcategoryChange(event: any) {
    this.categoryselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
    if (this.modulelist.length > 0) {
      this.modulelist = [];
      this.formData.patchValue({ courseModule: "" })
    }
    if (this.templatelist.length > 0) {
      this.templatelist = [];
      this.formData.patchValue({ courseTemplate: "" })
    }
    if (this.topicapi.length > 0) {
      this.topicapi = [];
      this.formData.patchValue({ courseTopic: "" })
    }
    console.log(this.coursecategorylist);
    this.coursecategorylist.forEach(element => {
      if (element._id.replace(/\s/g, "") == this.categoryselectedValue.replace(/\s/g, "")) {
        this.modulelist = element.moduleList;
        console.log(this.modulelist);
        if (Object.keys(this.paramData).length == 0) {
          //  this.formData.patchValue({ courseModule: element.moduleList[0]._id })
        }
      }
    });
  }

  isfromcoursedetailsgetmodule() {
    this.coursecategorylist.forEach(element => {
      if (element._id.replace(/\s/g, "") == this.categoryselectedValue.replace(/\s/g, "")) {
        this.modulelist = element.moduleList;
        if (Object.keys(this.paramData).length == 0) {
          this.formData.patchValue({ courseModule: this.moduleselectedValue })
        }
      }
    });

    this.isfromcoursedetailsgettemplate();
  }

  isfromcoursedetailsgettemplate() {
    var req = {
      moduleID: this.moduleselectedValue.replace(/\s/g, "")
    }
    this.categoryservice.getClassCourseTemplatefromModule(req).subscribe((result) => {
      this.templatelist = result.data.templateList;
      if (Object.keys(this.paramData).length == 0) {
        this.formData.patchValue({ courseTemplate: this.templateselectedValue })
      }
      this.isfromcoursedetailsgettopic();
    }, (err) => {
      console.log(err);
    });

  }

  isfromcoursedetailsgettopic() {
    console.log("templatelist", this.templatelist);
    this.templatelist.forEach(element => {
      if (element._id.replace(/\s/g, "") == this.templateselectedValue.replace(/\s/g, "")) {
        this.topicapi = element.topicList;
        if (Object.keys(this.paramData).length == 0) {
          this.formData.patchValue({ courseTopic: this.topicselectedValue })
        }
      }
    });
  }

  editmodulelist() {
    this.coursecategorylist.forEach(element => {
      if (element._id.replace(/\s/g, "") == this.categoryselectedValue.replace(/\s/g, "")) {
        this.modulelist = element.moduleList;
        if (Object.keys(this.paramData).length == 0) {
          this.formData.patchValue({ courseModule: this.moduleselectedValue })
        }
      }
    });
  }

  selectmoduleChange(event: any) {
    this.moduleselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
    if (this.templatelist.length > 0) {
      this.templatelist = [];
      this.formData.patchValue({ courseTemplate: "" })
    }
    if (this.topicapi.length > 0) {
      this.topicapi = [];
      this.formData.patchValue({ courseTopic: "" })
    }
    var req = {
      moduleID: this.moduleselectedValue.replace(/\s/g, "")
    }
    console.log("modulechange", req);
    this.categoryservice.getClassCourseTemplatefromModule(req).subscribe((result) => {
      this.templatelist = result.data.templateList;
      console.log("template", this.templatelist);
      if (Object.keys(this.paramData).length == 0) {
        // this.formData.patchValue({ courseCategory: "" })
      }
    }, (err) => {
      console.log(err);
    });

  }

  edittemplatelist() {
    var req = {
      moduleID: this.moduleselectedValue.replace(/\s/g, "")
    }
    this.categoryservice.getClassCourseTemplatefromModule(req).subscribe((result) => {
      this.templatelist = result.data.templateList;
      console.log(result.data.templateList);
      if (Object.keys(this.paramData).length == 0) {
        this.formData.patchValue({ courseTemplate: this.templateselectedValue })
      }
      this.edittopiclist();
    }, (err) => {
      console.log(err);
    });
  }

  selecttemplateChange(event: any) {
    this.templateselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
    if (this.topicapi.length > 0) {
      this.topicapi = [];
      this.formData.patchValue({ courseTopic: "" })
    }
    this.templatelist.forEach(element => {
      if (element._id.replace(/\s/g, "") == this.templateselectedValue.replace(/\s/g, "")) {
        this.topicapi = element.topicList;
        console.log("topic", this.topicapi);
        if (Object.keys(this.paramData).length == 0) {
          //  this.formData.patchValue({ courseModule: element.moduleList[0]._id })
        }
      }
    });
  }

  edittopiclist() {
    // if(this.topicapi.length > 0) {
    //   this.topicapi = [];
    //   this.formData.patchValue({ courseTopic: "" })
    // }
    this.templatelist.forEach(element => {
      if (element._id.replace(/\s/g, "") == this.templateselectedValue.replace(/\s/g, "")) {
        this.topicapi = element.topicList;
        if (Object.keys(this.paramData).length == 0) {
          this.formData.patchValue({ courseTopic: this.topicselectedValue })
        }
      }
    });
  }

  selectboardChange(event: any) {
    this.boardselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);

  }

  selectgradeChange(event: any) {
    this.gradeselectedValue = (event.target.options[event.target.selectedIndex].value).substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
    if (this.subjectselectedValue != "") {
      this.topiclist();
    }
  }

  selectlanguageChange(event: any) {
    this.languageselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
  }

  selectmaterialChange(event: any) {
    this.materialidselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
  }



  selectsubjectChange(event: any) {
    this.subjectselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);
    if (this.gradeselectedValue != "") {
      this.topiclist();
    }
  }

  selecttopicChange(event: any) {
    this.topicselectedValue = event.target.options[event.target.selectedIndex].value.substring(event.target.options[event.target.selectedIndex].value.indexOf(":") + 1);

  }

  goBack() {
    this.location.back();
  }

  // getPartner(){
  //   this.categoryservice.getAllPartner().subscribe((result) => {
  //     if(result.status){
  //       this.partnerList = result.data;  
  //       if(this.partnerList.length!=0){
  //         this.formData.patchValue({ partner: this.partnerList[0]._id })
  //       }
  //     }        
  //   }, (err) => {
  //   });
  // }

  getcoursecategory() {
    this.categoryservice.getClassCourseCategory().subscribe((result) => {
      this.coursecategorylist = result.data;
      if (this.isfromcoursedetails == true) {
        if (Object.keys(this.paramData).length == 0) {
          this.formData.patchValue({ courseCategory: this.categoryselectedValue })
        }
        this.isfromcoursedetailsgetmodule();
      }
      else {
        if (Object.keys(this.paramData).length == 0) {
          // this.formData.patchValue({ courseCategory: "" })
        }
      }

    }, (err) => {
      console.log(err);
    });
  }

  // getsubjectfield(){
  //   this.categoryservice.subjectfield().subscribe((result) => {
  //     this.coursesubjectlist = result.data;
  //     if(Object.keys(this.paramData).length == 0){

  //     }
  //     console.log(this.coursesubjectlist);
  //   }, (err) => {
  //   console.log(err);
  //   });
  // }

  topiclist() {
    var req = {
      subjectID: this.subjectselectedValue.replace(/\s/g, ""),
      gradeID: this.gradeselectedValue.replace(/\s/g, "")
    }
    const webToken = localStorage.getItem('webtoken');
    console.log("topicreq", req);
    this.categoryservice.topiclist(req, webToken).subscribe((result) => {
      this.topicapi = result.data;
      if (Object.keys(this.paramData).length == 0) {
        // this.formData.patchValue({ courseTopic: this.topicapi[0]._id })
      }
    }, (err) => {
      console.log(err);
    });
  }

}
