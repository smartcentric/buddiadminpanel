import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BuddiTestsComponent} from './buddi-tests.component';

const routes: Routes = [
  {
    path: '',
    component: BuddiTestsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BuddiTestsRoutingModule { }