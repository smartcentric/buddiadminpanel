import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicAlertRoutingModule } from './basic-alert-routing.module';
import { BasicAlertComponent } from './basic-alert.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';

import { MatNativeDateModule } from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from '@angular/material/input';
@NgModule({
  imports: [
    CommonModule,
    BasicAlertRoutingModule,
    SharedModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule,
    MatDatepickerModule, 
    MatNativeDateModule ,
    MatFormFieldModule,
    MatInputModule
  ],
  exports:[
    MatDatepickerModule, 
    MatNativeDateModule ,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [BasicAlertComponent]
})
export class BasicAlertModule { }
