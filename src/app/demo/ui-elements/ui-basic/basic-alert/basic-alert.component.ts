import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, } from '@angular/forms'
import * as XLSX from 'xlsx';  
import { ToastrManager } from 'ng6-toastr-notifications';
import { DateAdapter } from '@angular/material'
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-basic-alert',
  templateUrl: './basic-alert.component.html',
  styleUrls: ['./basic-alert.component.scss']
})
export class BasicAlertComponent implements OnInit {


  resultData: any = [];
  filterarrayproductname: [];
  term: string;
  p: number =1;
  categoryname : [];
  gradename : [];
  boardname : [];
  subjectname : [];
  languagename : [];
  length = 0;
  invoiceform: FormGroup;
  profilename : any;
  showModal : boolean;
  showModals : boolean;


  StudentfirstName : [];
  StudentlastName : [];
  StudentstudentID : [];
  StudentschoolName : [];
  Studentemail : [];
  StudentphoneNumber : [];
  StudentteacherID : [];
  // StudentgroupName : [];
  Studentaddress1 : [];
  Studentaddress2 : [];
  Studentarea : [];
  Studentcity : [];
  Studentcountry : [];
  Studentgender : [];

  productname : [];
  productid : [];
  productcode : [];
  productdescription : [];
  productprice : [];
  currency : [];
  totalclass : [];
  totalexam : [];
  // StudentstudentID : [];
  fileName= 'StudentPayments.xlsx';
  constructor(public invoice: FormBuilder,private dateAdapter: DateAdapter<any>,public datepipe: DatePipe,private categoryservice: RestapiService,private toastr: ToastrManager)
   { this.dateAdapter.setLocale('en-GB'); }

   setGerman() {
    this.dateAdapter.setLocale('en-GB');
  }
  onClick(event,user)
  {
    console.log('haimodal',user)
    this.showModals = true; // Show-Hide Modal Check
    this.productname = user.productDetails.productName;
    this.productid = user.productDetails.ProductListID;
    this.productcode = user.productDetails.productCode;
    this.productdescription = user.productDetails.productDescription;
    this.productprice = user.productDetails.productPrice;
    this.currency = user.productDetails.currency;
    this.totalclass = user.productDetails.totalClass;
    this.totalexam = user.productDetails.totalExam;
    // this.address1 = user.productDetails.address1;
    // this.address2 = user.teacherID.address2;
    // this.area = user.teacherID.area;
    // this.city = user.teacherID.city;
    // this.country = user.teacherID.country;


  
    console.log('haianothermodal',user)
     
  }

  onStudent(event,user){
    console.log('new student',user)
    this.showModal = true; // Show-Hide Modal Check
    this.StudentfirstName = user.userId.firstName;
    this.StudentlastName = user.userId.lastName;
    this.StudentstudentID = user.userId.studentID;
    this.StudentschoolName = user.userId.schoolName;
    this.Studentemail = user.userId.email;
    this.Studentgender = user.userId.gender;
    this.StudentphoneNumber = user.userId.phoneNumber;
    this.StudentstudentID = user.userId.studentID;
    // this.StudentgroupName = user.userId.groupName;
    this.Studentaddress1 = user.userId.address1;
    this.Studentaddress2 = user.userId.address2;
    this.Studentarea = user.userId.area;
    this.Studentcity = user.userId.city;
    this.Studentcountry = user.userId.country;
    
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }
  hides()
  {
    this.showModals = false;
  }


  ngOnInit() {
    // this.invoiceform = this.invoice.group({
    //   // userType : [''],
    //   startDate: ['', [Validators.required]],
    //   endDate: ['', [Validators.required]],

   
    // });
    var webToken = localStorage.getItem('webtoken');
    this.categoryservice.payments(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
        console.log('full data',this.resultData)
        // for(var i=0; i< this.resultData.length; i++){
        //   // this.productname = result.data.teacherID.modeofEngagement[0].employmentTyp;
        // }
        //  console.log('Teacher payment', result.data.teacherID.modeofEngagement[0].employmentType)
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
   
  }
  // get f() { return this.invoiceform.controls; }
  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  public onProductnameSelected(event) {
    const value = event.target.value;
  this.filterarrayproductname = [];
  for(var i=0; i< this.resultData.length; i++){
    this.filterarrayproductname = this.resultData[i].productDetails.filter(item =>item.productName.startsWith(value));
  }   
 }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'StudentPayments');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }

    submit(){
      if(this.invoiceform.invalid){
        this.toastr.errorToastr('Please Select Start and End Date');
        return;
      }
      const data = this.invoiceform.value;
      var req = {
        startDate:this.datepipe.transform(data.startDate, 'dd/MM/yyyy'),
        endDate: this.datepipe.transform(data.endDate, 'dd/MM/yyyy'),
      }
      console.log(req);
      console.log('start and end date',JSON.stringify(req));
  
    }
}
