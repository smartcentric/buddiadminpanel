import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-breadcrumb-paging',
  templateUrl: './breadcrumb-paging.component.html',
  styleUrls: ['./breadcrumb-paging.component.scss']
})
export class BreadcrumbPagingComponent implements OnInit {
   // resultData: [];
  
   term: string;
   p: number =1;
  //  categoryname : [];
  //  teachername : [];
  //  gradename : [];
  //  name : [];
  //  boardname : [];
  //  subjectname : [];
  //  languagename : [];
   
  firstName : [];
  // firstName : [];
  lastName : [];
  profileName : [];
  schoolName : [];
  email : [];
  phoneNumber : [];
  studentID : [];
  groupName : [];
  address1 : [];
  address2 : [];
  gender : [];
  area : [];
  city : [];
  country : [];
  showModal : boolean;
   public status: any = [];
   public resultData: any = [];
   length = 0;
   public statusfalsearray: any = [];
   
   profilename : any;
   fileName= 'oneononeClassPending.xlsx';
 
   constructor(private categoryservice: RestapiService) {
     
   }
   onClick(event,user)
   {
     console.log('haimodal',user)
     this.showModal = true; // Show-Hide Modal Check
     this.firstName = user.studentID.firstName;
     this.lastName = user.studentID.lastName;
     this.profileName = user.studentID.profileName;
     this.schoolName = user.studentID.schoolName;
     this.email = user.studentID.email;
     this.gender = user.studentID.gender;
     this.phoneNumber = user.studentID.phoneNumber;
     this.studentID = user.studentID.studentID;
     this.groupName = user.studentID.groupName;
     this.address1 = user.studentID.address1;
     this.address2 = user.studentID.address2;
     this.area = user.studentID.area;
     this.city = user.studentID.city;
     this.country = user.studentID.country;
     
     console.log('haianothermodal',user)
      
   }
   //Bootstrap Modal Close event
   hide()
   {
     this.showModal = false;
   }
   ngOnInit() {
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken)
    this.categoryservice.oneononeclass(webToken).subscribe((result) => {
      if (result) {
        //  this.resultData =  result.data;
       
         for(var i=0; i<result.data.length; i++){
          if (result.data[i].acceptStatus == false) {
            this.resultData.push(result.data[i]);
            this.length = this.resultData.length;
            
          }

      
       }
       
        } else {
         console.log('No Data');
        }
       });
  }
 
   key: string = 'profilename';
   reverse : boolean =false;
   sort(key){
     this.key = key;
     this.reverse = !this.reverse;
   }
 
   exportexcel(): void 
     {
        /* table id is passed over here */   
        let element = document.getElementById('excel-tableHide'); 
        const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
 
        /* generate workbook and add the worksheet */
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'oneononeClassPending');
 
        /* save to file */
        XLSX.writeFile(wb, this.fileName);
       
     }
}
