import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestUploadClassComponent } from './test-upload-class.component';

describe('TestUploadClassComponent', () => {
  let component: TestUploadClassComponent;
  let fixture: ComponentFixture<TestUploadClassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestUploadClassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestUploadClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
