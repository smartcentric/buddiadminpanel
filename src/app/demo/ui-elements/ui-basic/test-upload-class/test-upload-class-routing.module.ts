import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestUploadClassComponent} from './test-upload-class.component';

const routes: Routes = [
  {
    path: '',
    component: TestUploadClassComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestUploadClassRoutingModule { }