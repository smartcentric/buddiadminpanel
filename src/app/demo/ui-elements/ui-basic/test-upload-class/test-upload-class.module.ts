import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestUploadClassRoutingModule } from './test-upload-class-routing.module';
import { TestUploadClassComponent } from './test-upload-class.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    TestUploadClassRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [TestUploadClassComponent]
})
export class TestUploadClassModule { }
