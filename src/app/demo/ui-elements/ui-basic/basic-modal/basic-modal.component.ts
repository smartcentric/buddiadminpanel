import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-basic-modal',
  templateUrl: './basic-modal.component.html',
  styleUrls: ['./basic-modal.component.scss']
})
export class BasicModalComponent implements OnInit {

  resultData: [];
  term: string;
  p: number =1;
  // categoryname : [];
  // gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];
  length = 0;
  profilename : any;
  fileName= 'CoursesList.xlsx';
  constructor(private categoryservice: RestapiService) { }

  ngOnInit() {
    this.lists();
  
  }
  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'CoursesList');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }

  lists() {
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.list(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('lists', result.data)
         this.length = this.resultData.length;
      //    for(var i=0; i<result.data.length; i++){
      //     this.categoryname = result.data[i].categoryID.categoryName;
      //     this.gradename = result.data[i].gradeID.gradeName;
      //     this.boardname = result.data[i].boardID.boardName;
      //     this.languagename = result.data[i].languageID.languageName;
      //  this.subjectname = result.data[i].subjectID.subjectName;
      //  console.log('category audio',result.data[i].categoryID.categoryName)
      //  }
        } else {
         console.log('No Data');
        }
       });
  }


 


}
