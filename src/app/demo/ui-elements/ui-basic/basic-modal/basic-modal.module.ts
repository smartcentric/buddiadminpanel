import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicModalRoutingModule } from './basic-modal-routing.module';
import { BasicModalComponent } from './basic-modal.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbPopoverModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicModalRoutingModule,
    SharedModule,
    NgbPopoverModule,
    NgbTooltipModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ],
  declarations: [BasicModalComponent]
})
export class BasicModalModule { }
