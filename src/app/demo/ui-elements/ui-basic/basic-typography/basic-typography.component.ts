import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';

import * as XLSX from 'xlsx';
@Component({
  selector: 'app-basic-typography',
  templateUrl: './basic-typography.component.html',
  styleUrls: ['./basic-typography.component.scss']
})
export class BasicTypographyComponent implements OnInit {
  resultData: [];
  term: any;
  p: number =1;
  length = 0;
  profilename : any;
  fileName= 'SchoolRegister.xlsx';
  constructor(private categoryservice: RestapiService) { }

  ngOnInit() {
    this.Schoollists();
  }

  exportexcel(): void 
  {
     /* table id is passed over here */   
     let element = document.getElementById('excel-tableHide'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'SchoolRegister');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
    
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  Schoollists() {
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.Schoollist(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('School', result.data)
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
  }
}
