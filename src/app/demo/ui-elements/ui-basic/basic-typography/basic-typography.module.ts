import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicTypographyRoutingModule } from './basic-typography-routing.module';
import { BasicTypographyComponent } from './basic-typography.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicTypographyRoutingModule,
    SharedModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    OrderModule

  ],
  declarations: [BasicTypographyComponent]
})
export class BasicTypographyModule { }
