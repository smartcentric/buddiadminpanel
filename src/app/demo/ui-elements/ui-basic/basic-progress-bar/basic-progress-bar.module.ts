import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicProgressBarRoutingModule } from './basic-progress-bar-routing.module';
import { BasicProgressBarComponent } from './basic-progress-bar.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicProgressBarRoutingModule,
    SharedModule,
    NgbProgressbarModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ],
  declarations: [BasicProgressBarComponent]
})
export class BasicProgressBarModule { }
