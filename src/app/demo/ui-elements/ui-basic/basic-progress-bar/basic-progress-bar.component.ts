import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-basic-progress-bar',
  templateUrl: './basic-progress-bar.component.html',
  styleUrls: ['./basic-progress-bar.component.scss']
})
export class BasicProgressBarComponent implements OnInit {
  // resultData: [];
  
 
  // categoryname : [] ;
  // gradename : [];
  // name : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];
  firstName : [];
  lastName : [];
  profileName : [];
  schoolName : [];
  email : [];
  phoneNumber : [];
  teacherID : [];
  groupName : [];
  address1 : [];
  address2 : [];
  area : [];
  city : [];
  country : [];

  StudentfirstName : [];
  StudentlastName : [];
  StudentprofileName : [];
  StudentschoolName : [];
  Studentemail : [];
  StudentphoneNumber : [];
  StudentteacherID : [];
  StudentgroupName : [];
  Studentaddress1 : [];
  Studentaddress2 : [];
  Studentarea : [];
  Studentcity : [];
  Studentcountry : [];
  Studentgender : [];
  StudentstudentID : [];
  showModal : boolean;
  showModals : boolean;
  public status: any = [];
  public resultData: any = [];
  length = 0;
  public statusfalsearray: any = [];
  
  term: string;
  p: number =1;
  profilename : any;
  fileName= 'oneononeClassAccepted.xlsx';

  constructor(private categoryservice: RestapiService) {
    
  }
//   openModal(id: string) {
//     this.categoryservice.open(id);
//     console.log('id',this.categoryservice.open(id))
// }
  onClick(event,user)
  {
    console.log('haimodal',user)
    this.showModal = true; // Show-Hide Modal Check
    this.firstName = user.teacherID.firstName;
    this.lastName = user.teacherID.lastName;
    this.profileName = user.teacherID.profileName;
    this.schoolName = user.teacherID.schoolName;
    this.email = user.teacherID.email;
    this.phoneNumber = user.teacherID.phoneNumber;
    this.teacherID = user.teacherID.teacherID;
    this.groupName = user.teacherID.groupName;
    this.address1 = user.teacherID.address1;
    this.address2 = user.teacherID.address2;
    this.area = user.teacherID.area;
    this.city = user.teacherID.city;
    this.country = user.teacherID.country;


  
    console.log('haianothermodal',user)
     
  }

  onStudent(event,user){
    console.log('new student',user)
    this.showModals = true; // Show-Hide Modal Check
    this.StudentfirstName = user.studentID.firstName;
    this.StudentlastName = user.studentID.lastName;
    this.StudentprofileName = user.studentID.profileName;
    this.StudentschoolName = user.studentID.schoolName;
    this.Studentemail = user.studentID.email;
    this.Studentgender = user.studentID.gender;
    this.StudentphoneNumber = user.studentID.phoneNumber;
    this.StudentstudentID = user.studentID.studentID;
    this.StudentgroupName = user.studentID.groupName;
    this.Studentaddress1 = user.studentID.address1;
    this.Studentaddress2 = user.studentID.address2;
    this.Studentarea = user.studentID.area;
    this.Studentcity = user.studentID.city;
    this.Studentcountry = user.studentID.country;
    
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModals = false;
  }
  hides()
  {
    this.showModal = false;
  }

  ngOnInit() {
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken)
    this.categoryservice.oneononeclass(webToken).subscribe((result) => {
      if (result) {
        //  this.resultData =  result.data;
        console.log(result.data)
         
         for(var i=0; i<result.data.length; i++){
          if (result.data[i].acceptStatus == true) {
            this.resultData.push(result.data[i]);
            this.length = this.resultData.length;
     
          }

      
       }
       
        } else {
         console.log('No Data');
        }
       });

 
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'oneononeClassAccepted');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
}
