import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, } from '@angular/forms'
import * as XLSX from 'xlsx';  
import { ToastrManager } from 'ng6-toastr-notifications';
import { DateAdapter } from '@angular/material'
import { DatePipe } from '@angular/common'
import { isObservable } from 'rxjs';
@Component({
  selector: 'app-basic-other',
  templateUrl: './basic-other.component.html',
  styleUrls: ['./basic-other.component.scss']
})
export class BasicOtherComponent implements OnInit {
  datePickerCtrl = new FormControl();
  storeresult: any = [];
  resultData: any = [];
  filterarray: [];
  filterarrayteacher: [];
  filterarraysenior: [];
  filterarrayCity: [];
  filterarraymoe: [];
  term: string;
  date;
  invoiceform: FormGroup;
  p: number =1;
  productname : any;
  gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];
  length = 0;
  profilename : any;
  fileName= 'TeachersPayment.xlsx';
  constructor(private categoryservice: RestapiService,public datepipe: DatePipe, public invoice: FormBuilder,private dateAdapter: DateAdapter<any>,
    private toastr: ToastrManager) {
      this.dateAdapter.setLocale('en-GB'); 
     }

  ngOnInit() {
    // this.lists();
    this.invoiceform = this.invoice.group({
      // userType : [''],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],

   
    });
  
  }

  setGerman() {
    this.dateAdapter.setLocale('en-GB');
  }

  get f() { return this.invoiceform.controls; }
  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'TeachersPayment');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }

    public onOptionsSelected(event) {
      const value = event.target.value;
    this.filterarray = [];
    for(var i=0; i< this.resultData.length; i++){
      this.filterarray = this.resultData[i].paymentDetails.filter(item =>item.productName.startsWith(value));
    }   
   }

   public onSeniorJunior(event) {
    const value = event.target.value;
  this.filterarraysenior = [];
  for(var i=0; i< this.resultData.length; i++){
    this.filterarraysenior = this.resultData[i].paymentDetails.filter(item =>item.seniorOrJunior.includes(value));
  }
 }

   public onCity(event) {
    const value = event.target.value;
  this.filterarrayCity = [];
  for(var i=0; i< this.resultData.length; i++){
    this.filterarrayCity = this.resultData[i].requestDetails.filter(item =>item.city.includes(value));
  }
 }

   public onteachernameSelected(event) {
    const value = event.target.value;
  this.filterarrayteacher = [];
  for(var i=0; i< this.resultData.length; i++){
    this.filterarrayteacher = this.resultData[i](item =>item.classType.includes(value));
  }
 }

 public onOptionsSelectedTeacher(event) {
  const value = event.target.value;
  this.filterarrayteacher = [];
  for(var i=0; i< this.resultData.length; i++){
    this.filterarrayteacher = this.resultData[i].teacherID.filter(item =>item.profileName.includes(value));
  }
 }

 public onMOE(event) {
  const value = event.target.value;
this.filterarraymoe = [];
for(var i=0; i< this.resultData.length; i++){
  this.filterarraymoe = this.resultData[i](item =>item.classType.includes(value));
}
}

    submit(){
      if(this.invoiceform.invalid){
        this.toastr.errorToastr('Please Select Start and End Date');
        return;
      }
      const data = this.invoiceform.value;
      var req = {
        startDate:this.datepipe.transform(data.startDate, 'dd/MM/yyyy'),
        endDate: this.datepipe.transform(data.endDate, 'dd/MM/yyyy'),
      }
      console.log(req);
      console.log('start and end date',JSON.stringify(req));
      var webToken = localStorage.getItem('webtoken');
      this.categoryservice.teacherpayment(webToken,req).subscribe((result) => {
        if (result) {
          this.resultData = result.data;
          console.log('full data',this.resultData)
          // for(var i=0; i< this.resultData.length; i++){
          //   // this.productname = result.data.teacherID.modeofEngagement[0].employmentTyp;
          // }
          //  console.log('Teacher payment', result.data.teacherID.modeofEngagement[0].employmentType)
           this.length = this.resultData.length;
          } else {
           console.log('No Data');
          }
         });
    }

  // lists() {
  //   const webToken = localStorage.getItem('webtoken');
  //   this.categoryservice.teacherpayment(webToken).subscribe((result) => {
  //     if (result) {
  //       this.resultData = result.data;
  //        console.log('Teacher payment', result.data)
  //        this.length = this.resultData.length;
  //       } else {
  //        console.log('No Data');
  //       }
  //      });
  // }


}
