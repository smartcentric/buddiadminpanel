import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicOtherRoutingModule } from './basic-other-routing.module';
import { BasicOtherComponent } from './basic-other.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbPopoverModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { MatNativeDateModule } from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from '@angular/material/input';
@NgModule({
  imports: [
    CommonModule,
    BasicOtherRoutingModule,
    MatFormFieldModule,
    SharedModule,
    NgbTooltipModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule,
    MatDatepickerModule,
    MatNativeDateModule ,
    MatInputModule
  ],
  exports: [
    MatDatepickerModule, 
    MatNativeDateModule ,
    MatFormFieldModule,
    MatInputModule
],
  declarations: [BasicOtherComponent]
})
export class BasicOtherModule { }
