import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as XLSX from 'xlsx';  
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-basic-collapse',
  templateUrl: './basic-list.component.html',
  styleUrls: ['./basic-list.component.scss']
})
export class BasicListComponent implements OnInit {
  public isCollapsed: boolean;
  public multiCollapsed1: boolean;
  public multiCollapsed2: boolean;

  resultData: [];
  term: string;
  list : [];
  p: number =1;
  length = 0;
  category : [];
  categoryname : [];
  gradename : [];
  boardname : [];
  subjectname : [];
  languagename : [];

  fileName= 'AudioList.xlsx';
  constructor(public toastr: ToastrManager,private categoryservice: RestapiService,private router: Router) {

  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-table'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'AudioList');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
  ngOnInit() {
    this.isCollapsed = true;
    this.multiCollapsed1 = true;
    this.multiCollapsed2 = true;
 var urlArray = window.location.href.split(':');
   
     var id=urlArray[urlArray.length-1];
    
    this.listDetail(id);
  }
editlist(id){
   
var id=id;
  
 
   this.router.navigate(['/basic/editlistsession/:'+id]);
}
 
 //deletelist(id){
 //var rst={
  //  id:id,
 // }
//this.categoryservice.deletesession(rst).subscribe((result) => {
   //console.log(result);
  //this.toastr.successToastr('Hooray! Your data has been removed');

  // })
 //}






  listDetail(id) {
  var rst={
id:id,
  }
    this.categoryservice.getsession(rst).subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         
         console.log('audio list', this.resultData);
        for(var i=0; i<this.resultData.length; i++){
          this.categoryname = result.data[i].categoryID.categoryName;
          this.gradename = result.data[i].gradeID.gradeName;
          this.boardname = result.data[i].boardID.boardName;
          this.languagename = result.data[i].languageID.languageName;
          this.subjectname = result.data[i].subjectID.subjectName;
          // console.log('category audio',result.data[i].categoryID.categoryName)
        }
        } else {
         console.log('No Data');
        }
       });
  }
  
}
