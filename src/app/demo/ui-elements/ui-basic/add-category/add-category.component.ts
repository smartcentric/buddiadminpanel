import { Component, OnInit } from "@angular/core";
import { RestapiService } from "../../../../restapi.service";
import { ToastrManager } from "ng6-toastr-notifications";
import {
  FormBuilder,
  FormGroup,
  FormArray,
  FormControl,
  Validators,
  ValidatorFn,
} from "@angular/forms";
import { Location } from "@angular/common";

@Component({
  selector: "app-add-category",
  templateUrl: "./add-category.component.html",
  styleUrls: ["./add-category.component.scss"],
})
export class AddCategoryComponent implements OnInit {
  testForm: FormGroup;
  submitted = false;
  formData;
  selectedFileName: string = "Choose File";

  constructor(
    private categoryservice: RestapiService,
    public location: Location,
    public toastr: ToastrManager,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.testForm = this.formBuilder.group({
      category: ["", Validators.required],
      categoryid: ["", Validators.required],
      // fileupload: ['', Validators.required],
      // upload_name: [null],
    });
  }

  get f() {
    return this.testForm.controls;
  }

  backto() {
    this.location.back();
  }

  fileChange1(event) {
    let fileList: FileList = event.target.files;
    // const file = event.target.files[0];
    // console.log(fileList);
    this.testForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileName = fileList[0].name;
    console.log(this.selectedFileName);
    this.testForm.patchValue({ fileupload: this.selectedFileName });
    // this.testForm.get('fileupload').setValue(file);
  }

  onSubmit() {
    var btn = document.getElementById("addCategoryButton") as HTMLButtonElement;
    btn.disabled = true;
    this.submitted = true;
    let data = this.testForm.value;
    if (!this.testForm.valid) {
      const invalid = [];
      const controls = this.testForm.controls;
      btn.disabled = false;
      for (const name in controls) {
        if (controls[name].invalid) {
          if (name == "category") {
            this.toastr.warningToastr("category required");
          } else {
            this.toastr.warningToastr("Invalid Fields");
          }
        }
        return;
      }
    }

    var req = {
      categoryName: data.category,
      categoryID: data.categoryid,
      status: true,
      image: "",
    };
    //  this.formData = new FormData();
    //  this.formData.append("categoryName", data.category);
    //  this.formData.append("categoryID", data.categoryid);
    //  this.formData.append("status",true)
    //  this.formData.append("image", data.upload_name, data.upload_name.name);
    var token = localStorage.getItem("webtoken");

    this.categoryservice.AdminAddCategory(req, token).subscribe((response) => {
      console.log(response);
      if (response.message == "Category saved") {
        this.toastr.successToastr(response.message);
        this.backto();
      } else {
        btn.disabled = false;
        this.toastr.errorToastr(response.message);
      }
    });
  }
}
