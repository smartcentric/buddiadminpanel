import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCategoryRoutingModule } from './add-category-routing.module';
import { AddCategoryComponent } from './add-category.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';


@NgModule({
  imports: [
    CommonModule,
    AddCategoryRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [AddCategoryComponent]
})
export class AddCategoryModule { }
