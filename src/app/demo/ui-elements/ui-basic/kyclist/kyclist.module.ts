import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KyclistRoutingModule } from './kyclist-routing.module';
import {KyclistComponent} from './kyclist.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [KyclistComponent],
  imports: [
    SharedModule,
    FormsModule,
    NgbCollapseModule,
    NgbAccordionModule,
    CommonModule,
    KyclistRoutingModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ]
})
export class KyclistModule { }
