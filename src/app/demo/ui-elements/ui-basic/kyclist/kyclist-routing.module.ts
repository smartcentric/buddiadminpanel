import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {KyclistComponent} from './kyclist.component'

const routes: Routes = [
  {
    path: '',
    component: KyclistComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KyclistRoutingModule { }
