import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KycAcceptedRoutingModule } from './kyc-accepted-routing.module';
import {KycAcceptedComponent} from './kyc-accepted.component'
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { FormsModule } from '@angular/forms';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  declarations: [KycAcceptedComponent],
  imports: [
    CommonModule,
    KycAcceptedRoutingModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule,
    FormsModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
  ]
})
export class KycAcceptedModule { }
