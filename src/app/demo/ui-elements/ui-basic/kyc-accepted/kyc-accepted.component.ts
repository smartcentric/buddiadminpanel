import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-kyc-accepted',
  templateUrl: './kyc-accepted.component.html',
  styleUrls: ['./kyc-accepted.component.scss']
})
export class KycAcceptedComponent implements OnInit {
  public resultData: any = [];
  length = 0;
  term: string;
  p: number =1;
  profilename : any;
  fileName= 'KYCAccepted.xlsx';
  constructor(private categoryservice: RestapiService) { }

  ngOnInit(): void {
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken)
    this.categoryservice.newapi(webToken).subscribe((result) => {
      if (result.data.accepted) {
         this.resultData =  result.data.accepted;
         this.length = result.data.accepted.length;
        console.log(result.data)
       
        } else {
         console.log('No Data');
        }
       });

  }

  exportexcel(): void 
  {
     /* table id is passed over here */   
     let element = document.getElementById('excel-tableHide'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'KYCAccepted');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
    
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

}
