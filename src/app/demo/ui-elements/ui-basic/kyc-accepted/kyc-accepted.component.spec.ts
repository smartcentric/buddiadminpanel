import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycAcceptedComponent } from './kyc-accepted.component';

describe('KycAcceptedComponent', () => {
  let component: KycAcceptedComponent;
  let fixture: ComponentFixture<KycAcceptedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycAcceptedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycAcceptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
