import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-kyc-rejected',
  templateUrl: './kyc-rejected.component.html',
  styleUrls: ['./kyc-rejected.component.scss']
})
export class KycRejectedComponent implements OnInit {
  public resultData: any = [];
  length = 0;
  term: string;
  p: number =1;
  profilename : any;
  fileName= 'KYCRejected.xlsx';
  constructor(private categoryservice: RestapiService) { }

  ngOnInit(): void {
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken)
    this.categoryservice.newapi(webToken).subscribe((result) => {
      if (result.data.rejected) {
         this.resultData =  result.data.rejected;
         this.length = result.data.rejected.length;
        console.log(result.data)
       
        } else {
         console.log('No Data');
        }
       });

  }

  exportexcel(): void 
  {
     /* table id is passed over here */   
     let element = document.getElementById('excel-table'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'KYCRejected');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
    
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

}
