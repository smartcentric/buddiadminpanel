import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycRejectedComponent } from './kyc-rejected.component';

describe('KycRejectedComponent', () => {
  let component: KycRejectedComponent;
  let fixture: ComponentFixture<KycRejectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycRejectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycRejectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
