import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {RestapiService} from '../../../../restapi.service';

import * as XLSX from 'xlsx';
@Component({
  selector: 'app-basic-tabs-pills',
  templateUrl: './basic-tabs-pills.component.html',
  styleUrls: ['./basic-tabs-pills.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BasicTabsPillsComponent implements OnInit {
  resultData: [];
  term: any;
  p: number =1;
  length = 0;
  profilename : any;
  fileName= 'ContactUsRegister.xlsx';
  constructor(private categoryservice: RestapiService) { }

  exportexcel(): void 
  {
     /* table id is passed over here */   
     let element = document.getElementById('excel-tableHide'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'ContactUsRegister');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
    
  }

  ngOnInit() {
    this.contactlists();
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }


  contactlists() {
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.Contactlist(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('contact', result.data);
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
  }
}
