import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicTabsPillsRoutingModule } from './basic-tabs-pills-routing.module';
import { BasicTabsPillsComponent } from './basic-tabs-pills.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicTabsPillsRoutingModule,
    SharedModule,
    NgbTabsetModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    OrderModule
  ],
  declarations: [BasicTabsPillsComponent]
})
export class BasicTabsPillsModule { }
