import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeacherSmartmaterialApproveRoutingModule } from './teacher-smartmaterial-approve-routing.module';
import {TeacherSmartmaterialApproveComponent} from './teacher-smartmaterial-approve.component'
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { FormsModule } from '@angular/forms';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';


@NgModule({
  declarations: [TeacherSmartmaterialApproveComponent],
  imports: [
    CommonModule,
    TeacherSmartmaterialApproveRoutingModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule,
    FormsModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
  ]
})
export class TeacherSmartmaterialApproveModule { }
