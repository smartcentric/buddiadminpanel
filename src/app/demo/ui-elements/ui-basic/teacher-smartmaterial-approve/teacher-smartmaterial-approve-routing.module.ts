import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TeacherSmartmaterialApproveComponent } from './teacher-smartmaterial-approve.component';

const routes: Routes = [{ path: '', component: TeacherSmartmaterialApproveComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherSmartmaterialApproveRoutingModule { }
