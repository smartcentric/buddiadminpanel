import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as XLSX from 'xlsx';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-teacher-smartmaterial-approve',
  templateUrl: './teacher-smartmaterial-approve.component.html',
  styleUrls: ['./teacher-smartmaterial-approve.component.scss']
})
export class TeacherSmartmaterialApproveComponent implements OnInit {
  public resultData: any = [];
  length = 0;
  term: string;
  p: number =1;
  profilename : any;
  public materialname = "";
  public date = "";
  public teachername = "";
  public materialid: any;
  constructor(private categoryservice: RestapiService,public toastr: ToastrManager,private router: Router) { 
    this.router.routeReuseStrategy.shouldReuseRoute = function() {
      return false;
  };
  }

  ngOnInit(): void {
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken);
    this.categoryservice.getteacheruploadedsmartmaterial(webToken).subscribe((result) => {
      if (result.status) {
         this.resultData =  result.data;
         this.length = result.data.length;
        console.log(result.data)
       
        } else {
         console.log('No Data');
        }
       });
  }
  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  openUrl(url: string) {
    if (url != "" && url != undefined) {
        window.open(url, "_blank");  
    }    
  }


  smartmaterialdata(materialname,teachername,date,id) {
    this.materialname = materialname;
    this.teachername = teachername;
    this.date = date;
    this.materialid = id;
      }

  rejectvideo() {
    var reason= ((document.getElementById("reject") as HTMLInputElement).value); 
   
   // this.confirmapproval("reject");
    if(reason == null || reason == "") {
      this.toastr.errorToastr("Reason for Rejection required");
    }
    else {
      var req = {
        materialID : this.materialid,
        status : "Reject",
        reasonForRejection : reason
      }
      this.confirmapproval(req);
    }
  }

  acceptvideo() {
    var req = {
      materialID : this.materialid,
      status : "Publish"
    }
    this.confirmapproval(req);
  }

  confirmapproval(data) {
    console.log(data);
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken);
    this.categoryservice.Updatesmartmaterialstatus(data, webToken).subscribe((result) => {
      if(result.status) {
        this.toastr.successToastr(result.message);
        window.location.reload();
    }
      else {
        this.toastr.errorToastr(result.message);
      }
    }, (err) => {
  console.log(err);
  });
  }

}
