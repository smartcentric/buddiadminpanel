import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherSmartmaterialApproveComponent } from './teacher-smartmaterial-approve.component';

describe('TeacherSmartmaterialApproveComponent', () => {
  let component: TeacherSmartmaterialApproveComponent;
  let fixture: ComponentFixture<TeacherSmartmaterialApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherSmartmaterialApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherSmartmaterialApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
