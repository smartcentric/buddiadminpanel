import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadClassCourseComponent } from './upload-class-course.component';

describe('UploadClassCourseComponent', () => {
  let component: UploadClassCourseComponent;
  let fixture: ComponentFixture<UploadClassCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadClassCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadClassCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
