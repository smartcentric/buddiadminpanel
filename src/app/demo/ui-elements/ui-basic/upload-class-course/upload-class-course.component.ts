import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service'
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-upload-class-course',
  templateUrl: './upload-class-course.component.html',
  styleUrls: ['./upload-class-course.component.scss']
})
export class UploadClassCourseComponent implements OnInit {
  createForm: FormGroup;
  selectedFileName: string = 'Choose File';
  selectedThumbnailName: string = 'Choose File';
  submitted = false;
  formData; 

  constructor(private categoryservice: RestapiService , public location: Location,public toastr: ToastrManager,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.createForm = this.formBuilder.group({    
      fileupload: ['', Validators.required],
      upload_name: [null],
      thumbnailupload: ['', Validators.required],
      thumbnail_name: [null]

    });
  }
  get f() {
    return this.createForm.controls;
  }

  backto(){
    this.location.back();
  }


  fileChange(event) {
    let fileList: FileList = event.target.files;
   // const file = event.target.files[0];
   // console.log(fileList);
   console.log(fileList[0]);
    this.createForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileName = fileList[0].name;
    this.createForm.patchValue({ fileupload: this.selectedFileName });
   // this.testForm.get('fileupload').setValue(file);
  }

  thumbnailchange(event) {
    let fileList: FileList = event.target.files;
   console.log(fileList[0]);
    this.createForm.patchValue({ thumbnail_name: fileList[0] });
    this.selectedThumbnailName = fileList[0].name;
    this.createForm.patchValue({ thumbnailupload: this.selectedFileName });
   // this.testForm.get('fileupload').setValue(file);
  }

  onSubmit() {
    var btn = (document.getElementById('submitButton') as HTMLButtonElement);
    btn.disabled = true;
    this.submitted = true;
    let data = this.createForm.value;
    if (!this.createForm.valid) {
      btn.disabled = false;
      const invalid = [];
    const controls = this.createForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        if(name == "fileupload") {
          this.toastr.warningToastr("Excel file required")
        }
        else if(name == "thumbnailupload") {
          this.toastr.warningToastr("Thumbnail file required")
        }
        else {
          this.toastr.warningToastr("Invalid Fields")
        }
        }
      
       return;
      }
    }


   this.formData = new FormData();
   this.formData.append("excel_file", data.upload_name);
   this.formData.append("thumbnail_file", data.thumbnail_name);
   var token = localStorage.getItem('webtoken');

//    for (var pair of this.formData.entries()) {
//     console.log(pair[0]+ ', ' + pair[1]); 
// }
   this.categoryservice.uploadCourseTemplate(this.formData,token).subscribe((response) => {
    if(response.status){
      this.toastr.successToastr(response.message);
      this.backto()
    
    }
    else {
      btn.disabled = false;
     this.toastr.errorToastr(response.message);
    }
  });  
  }
}
