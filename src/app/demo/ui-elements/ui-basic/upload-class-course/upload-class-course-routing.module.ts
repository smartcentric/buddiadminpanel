import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UploadClassCourseComponent} from './upload-class-course.component';

const routes: Routes = [
  {
    path: '',
    component: UploadClassCourseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UploadClassCourseRoutingModule { }