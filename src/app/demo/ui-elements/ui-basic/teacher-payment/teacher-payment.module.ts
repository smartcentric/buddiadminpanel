import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeacherPaymentRoutingModule } from './teacher-payment-routing.module';
import {TeacherPaymentComponent} from './teacher-payment.component';

import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { FormsModule } from '@angular/forms';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { from } from 'rxjs';
@NgModule({
  declarations: [TeacherPaymentComponent],
  imports: [
    CommonModule,
    TeacherPaymentRoutingModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule,
    FormsModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
  ]
})
export class TeacherPaymentModule { }
