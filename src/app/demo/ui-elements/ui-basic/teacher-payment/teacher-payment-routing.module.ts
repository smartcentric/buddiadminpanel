import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {TeacherPaymentComponent} from './teacher-payment.component';
const routes: Routes = [
  {
    path: '',
    component: TeacherPaymentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherPaymentRoutingModule { }
