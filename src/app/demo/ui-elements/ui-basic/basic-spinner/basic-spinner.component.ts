import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';

import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-basic-spinner',
  templateUrl: './basic-spinner.component.html',
  styleUrls: ['./basic-spinner.component.scss']
})
export class BasicSpinnerComponent implements OnInit {
  public btnLoader: boolean;
  public submitLoader: boolean;
  resultData: [];
  term: any;
  p: number =1;
  profilename : any;
  // categoryname : [];
  // gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];
  length =0;
  fileName= 'ExamsList.xlsx';
  constructor(private categoryservice: RestapiService) {
    this.btnLoader = false;
    this.submitLoader = false;
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'ExamsList');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }

  ngOnInit() {
    this.exams();
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }



  exams() {
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.exam(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('exams', result.data)
         this.length = this.resultData.length;
      //    for(var i=0; i<result.data.length; i++){
      //     this.categoryname = result.data[i].categoryID.categoryName;
      //     this.gradename = result.data[i].gradeID.gradeName;
      //     this.boardname = result.data[i].boardID.boardName;
      //     this.languagename = result.data[i].languageID.languageName;
      //  this.subjectname = result.data[i].subjectID.subjectName;
      //  console.log('category audio',result.data[i].categoryID.categoryName)
      //  }
        } else {
         console.log('No Data');
        }
       });
  }

  onBtnLoader() {
    this.btnLoader = true;
    setTimeout(() => {
      this.btnLoader = false;
    }, 2000);
  }

  onSubmitLoader() {
    this.submitLoader = true;
    setTimeout(() => {
      this.submitLoader = false;
    }, 2000);
  }

}
