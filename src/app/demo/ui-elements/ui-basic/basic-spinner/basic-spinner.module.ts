import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicSpinnerRoutingModule } from './basic-spinner-routing.module';
import { BasicSpinnerComponent } from './basic-spinner.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  declarations: [BasicSpinnerComponent],
  imports: [
    CommonModule,
    BasicSpinnerRoutingModule,
    SharedModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ]
})
export class BasicSpinnerModule { }
