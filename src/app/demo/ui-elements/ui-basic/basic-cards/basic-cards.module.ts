import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicCardsRoutingModule } from './basic-cards-routing.module';
import { BasicCardsComponent } from './basic-cards.component';
import {SharedModule} from '../../../../theme/shared/shared.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicCardsRoutingModule,
    SharedModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    OrderModule
  ],
  declarations: [BasicCardsComponent]
})
export class BasicCardsModule { }
