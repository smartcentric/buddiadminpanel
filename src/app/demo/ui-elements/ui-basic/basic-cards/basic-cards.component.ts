import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-basic-cards',
  templateUrl: './basic-cards.component.html',
  styleUrls: ['./basic-cards.component.scss']
})
export class BasicCardsComponent implements OnInit {

  resultData: [];
  term: string;
  p: number =1;
  profilename : any;
  // categoryname : [];
  // gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];

  firstName : [];
  // firstName : [];
  lastName : [];
  profileName : [];
  schoolName : [];
  email : [];
  phoneNumber : [];
  teacherID : [];
  groupName : [];
  address1 : [];
  address2 : [];
  area : [];
  city : [];
  country : [];
  showModal : boolean;
  length = 0;
  registerForm: FormGroup;
  selectedFileNmae: string = 'Upload Photo';
  fileName= 'ClassesList.xlsx';
  
  constructor(public toastr: ToastrManager,private categoryservice: RestapiService,private formBuilder: FormBuilder) { }
  
  
  onClick(event,user)
  {
    console.log('haimodal',user)
    this.showModal = true; // Show-Hide Modal Check
    this.firstName = user.teacherID.firstName;
    this.lastName = user.teacherID.lastName;
    this.profileName = user.teacherID.profileName;
    this.schoolName = user.teacherID.schoolName;
    this.email = user.teacherID.email;
    this.phoneNumber = user.teacherID.phoneNumber;
    this.teacherID = user.teacherID.teacherID;
    this.groupName = user.teacherID.groupName;
    this.address1 = user.teacherID.address1;
    this.address2 = user.teacherID.address2;
    this.area = user.teacherID.area;
    this.city = user.teacherID.city;
    this.country = user.teacherID.country;
    
    console.log('haianothermodal',user)
     
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }
  
  exportexcel(): void 
  {
     /* table id is passed over here */   
     let element = document.getElementById('excel-tableHide'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'ClassesList');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
    
  }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      categoryName: ['', Validators.required],
      image: ['', Validators.required],
      categoryID: ['', Validators.required],
  });
    this.listCourseCategory();
    const webToken = localStorage.getItem('webtoken');
    console.log(webToken)
    this.categoryservice.oneononeclass(webToken).subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('one on one class', this.resultData)
         this.length = this.resultData.length;
       
        } else {
         console.log('No Data');
        }
       });
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  get f() { return this.registerForm.controls; }

  fileChange(event) {
    let fileList: FileList = event.target.files;

    this.registerForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileNmae = fileList[0].name;
    this.registerForm.patchValue({ image: this.selectedFileNmae });
    console.log('upload photi',this.selectedFileNmae)

  }

  onSubmit(){
    if(this.registerForm.valid){
      const data = this.registerForm.value;
      this.categoryservice.addcourse(data).subscribe(res => {
        if(res.status){
          console.log('success')
        }
        else{
          console.log('failure')
        }
      });
    }
    }

  listCourseCategory() {
    this.categoryservice.classList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('class list', this.resultData)
         this.length = this.resultData.length;
        //  for(var i=0; i<result.data.length; i++){
        //   this.categoryname = result.data[i].categoryID.categoryName;
        //   this.gradename = result.data[i].gradeID.gradeName;
        //   this.boardname = result.data[i].boardID.boardName;
        //   this.languagename = result.data[i].languageID.languageName;
        //   this.subjectname = result.data[i].subjectID.subjectName;
        //   // console.log('category audio',result.data[i].categoryID.categoryName)
        // }
        } else {
         console.log('No Data');
        }
       });
  }

  deleteRemainder(data){
    if(confirm("Are you sure to delete?")) {
      console.log(data);
      //**Passing webtoken and lifeid to backend */
      
      var passJson = data._id
      console.log('sa',passJson)
      this.categoryservice.deleteCourses(passJson).subscribe((result) => {
       if (result.status) {
          this.toastr.errorToastr('Categorys deleted','Success',{
             position:'top-right'
          });
          this.ngOnInit();
        }
       }, (err) => {
       console.log(err);
       });
    }
   }

}
