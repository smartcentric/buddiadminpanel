import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {KyclistComponent} from './kyclist/kyclist.component'
import {KycAcceptedComponent} from './kyc-accepted/kyc-accepted.component';
import {KycRejectedComponent} from './kyc-rejected/kyc-rejected.component';
import {UpiPendingComponent} from './upi-pending/upi-pending.component';
import {UpiAcceptComponent} from './upi-accept/upi-accept.component';
import { BasicRazorpayComponent } from './basic-razorpay/basic-razorpay.component';
import {TeacherPaymentComponent} from './teacher-payment/teacher-payment.component';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'alert',
        loadChildren: () => import('./basic-alert/basic-alert.module').then(module => module.BasicAlertModule)
      },
      {
        path: 'button',
        loadChildren: () => import('./basic-button/basic-button.module').then(module => module.BasicButtonModule)
      },
      {
        path: 'badges',
        loadChildren: () => import('./basic-badge/basic-badge.module').then(module => module.BasicBadgeModule)
      },
      {
        path: 'breadcrumb-paging',
        loadChildren: () => import('./breadcrumb-paging/breadcrumb-paging.module').then(module => module.BreadcrumbPagingModule)
      },
      {
        path: 'cards',
        loadChildren: () => import('./basic-cards/basic-cards.module').then(module => module.BasicCardsModule)
      },
      {
        path: 'collapse',
        loadChildren: () => import('./basic-collapse/basic-collapse.module').then(module =>module.BasicCollapseModule)
      },
     {
       path: 'tableview',
        loadChildren: () => import('./basic-detail/basic-detail.module').then(module => module.BasicDetailModule)
     },
     {
      path: 'buddiTests',
       loadChildren: () => import('./buddi-tests/buddi-tests.module').then(module => module.BuddiTestsModule)
    },
    {
      path: 'testuploadtopic',
       loadChildren: () => import('./test-upload-topic/test-upload-topic.module').then(module => module.TestUploadTopicModule)
    },
    {
      path: 'testuploadclass',
       loadChildren: () => import('./test-upload-class/test-upload-class.module').then(module => module.TestUploadClassModule)
    },
    {
      path: 'testuploadclassdocument',
       loadChildren: () => import('./test-upload-class-document/test-upload-class-document.module').then(module => module.TestUploadClassDocumentModule)
    },
    {
      path: 'testuploadvideo',
       loadChildren: () => import('./test-upload-video/test-upload-video.module').then(module => module.TestUploadVideoModule)
    },
    {
      path: 'uploadclasscourse',
       loadChildren: () => import('./upload-class-course/upload-class-course.module').then(module => module.UploadClassCourseModule)
    },
      {
       path: 'sessiondetail/:id',
        loadChildren: () => import('./basic-list/basic-list.module').then(module => module.BasicListModule)
     },
     {
      path: 'Approvevideoclass',
       loadChildren: () => import('./teacher-class-video-approve/teacher-class-video-approve.module').then(module => module.TeacherClassVideoApproveModule)
    },
    {
      path: 'Approvesmartmaterial',
       loadChildren: () => import('./teacher-smartmaterial-approve/teacher-smartmaterial-approve.module').then(module => module.TeacherSmartmaterialApproveModule)
    },
    {
      path: 'add-category',
       loadChildren: () => import('./add-category/add-category.module').then(module => module.AddCategoryModule)
    },
    {
      path: 'delete-category',
       loadChildren: () => import('./delete-category/delete-category.module').then(module => module.DeleteCategoryModule)
    },
    {
      path: 'upload-student',
       loadChildren: () => import('./upload-student/upload-student.module').then(module => module.UploadStudentModule)
    },
    {
      path: 'upload-teacher',
      loadChildren: () => import('./upload-teacher/upload-teacher.module').then(module => module.UploadTeacherModule)
    },
     {
      path: 'kyclistPending',
      component : KyclistComponent
    },
    {
      path: 'kyclistAccepted',
      component : KycAcceptedComponent
    },
    {
      path: 'kyclistRejected',
      component: KycRejectedComponent
    },
    {
      path: 'upilistPending',
      component : UpiPendingComponent
    },
    {
      path: 'upilistAccept',
      component : UpiAcceptComponent
    },
    {
      path: 'razlistrazor',
      component : BasicRazorpayComponent
    },
    {
      path: 'TeacherPayment',
      component: TeacherPaymentComponent
    },
      
     
      {
       path: 'editlistsession/:id',
        loadChildren: () => import('../../pages/form-elements/editsession-elements/editsession-elements.module').then(module => module.EditSessionElementsModule)
     },
      {
        path: 'carousel',
        loadChildren: () => import('./basic-carousel/basic-carousel.module').then(module => module.BasicCarouselModule)
      },
      {
        path: 'grid-system',
        loadChildren: () => import('./basic-grid/basic-grid.module').then(module => module.BasicGridModule)
      },
      {
        path: 'progress',
        loadChildren: () => import('./basic-progress-bar/basic-progress-bar.module').then(module => module.BasicProgressBarModule)
      },
      {
        path: 'razorpay',
        loadChildren: () => import('./basic-razorpay/basic-razorpay.module').then(module => module.BasicRazorpayModule)
      },
      {
        path: 'modal',
        loadChildren: () => import('./basic-modal/basic-modal.module').then(module => module.BasicModalModule)
      },
      {
        path: 'spinner',
        loadChildren: () => import('./basic-spinner/basic-spinner.module').then(module => module.BasicSpinnerModule)
      },
      {
        path: 'tabs-pills',
        loadChildren: () => import('./basic-tabs-pills/basic-tabs-pills.module').then(module => module.BasicTabsPillsModule)
      },
      {
        path: 'typography',
        loadChildren: () => import('./basic-typography/basic-typography.module').then(module => module.BasicTypographyModule)
      },
      {
        path: 'tooltip-popovers',
        loadChildren: () => import('./basic-tooltip-popovers/basic-tooltip-popovers.module')
          .then(module => module.BasicTooltipPopoversModule)
      },
      {
        path: 'other',
        loadChildren: () => import('./basic-other/basic-other.module').then(module => module.BasicOtherModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UiBasicRoutingModule { }
