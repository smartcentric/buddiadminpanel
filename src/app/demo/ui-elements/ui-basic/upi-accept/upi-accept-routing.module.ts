import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UpiAcceptComponent} from './upi-accept.component';

const routes: Routes = [
  {
    path: '',
    component: UpiAcceptComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpiAcceptgRoutingModule { }