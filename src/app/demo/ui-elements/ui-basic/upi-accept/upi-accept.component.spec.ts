import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpiAcceptComponent } from './upi-accept.component';

describe('UpiAcceptComponent', () => {
  let component: UpiAcceptComponent;
  let fixture: ComponentFixture<UpiAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpiAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpiAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
