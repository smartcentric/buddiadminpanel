import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicTooltipPopoversRoutingModule } from './basic-tooltip-popovers-routing.module';
import { BasicTooltipPopoversComponent } from './basic-tooltip-popovers.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbPopoverModule, NgbTooltipModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicTooltipPopoversRoutingModule,
    SharedModule,
    NgbTooltipModule,
    NgbPopoverModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    OrderModule
  ],
  declarations: [BasicTooltipPopoversComponent]
})
export class BasicTooltipPopoversModule { }
