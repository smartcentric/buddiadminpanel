import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';

import * as XLSX from 'xlsx';
@Component({
  selector: 'app-basic-tooltip-popovers',
  templateUrl: './basic-tooltip-popovers.component.html',
  styleUrls: ['./basic-tooltip-popovers.component.scss']
})
export class BasicTooltipPopoversComponent implements OnInit {
  resultData: [];
  term: any;
  p: number =1;
  length = 0;
  profilename : any;
  fileName= 'StudentRegister.xlsx';
  constructor(private categoryservice: RestapiService) { }

  exportexcel(): void 
  {
     /* table id is passed over here */   
     let element = document.getElementById('excel-tableHide'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     /* generate workbook and add the worksheet */
     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'StudentRegister');

     /* save to file */
     XLSX.writeFile(wb, this.fileName);
    
  }

  ngOnInit() {
    this.studentlists();
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  studentlists() {
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.Studentlist(webToken).subscribe((result) => {
      if (result) {
        this.resultData = result.data;
         console.log('Student', result.data)
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
  }
}
