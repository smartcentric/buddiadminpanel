import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicCollapseRoutingModule } from './basic-collapse-routing.module';
import { BasicCollapseComponent } from './basic-collapse.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbAccordionModule, NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  imports: [
    CommonModule,
    BasicCollapseRoutingModule,
    SharedModule,
    NgbCollapseModule,
    NgbAccordionModule,
    Ng2SearchPipeModule,
    OrderModule,
    NgxPaginationModule
  ],
  declarations: [BasicCollapseComponent]
})
export class BasicCollapseModule { }
