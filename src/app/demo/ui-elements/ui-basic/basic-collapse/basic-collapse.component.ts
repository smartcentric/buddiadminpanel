import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import * as XLSX from 'xlsx';  
@Component({
  selector: 'app-basic-collapse',
  templateUrl: './basic-collapse.component.html',
  styleUrls: ['./basic-collapse.component.scss']
})
export class BasicCollapseComponent implements OnInit {
  public isCollapsed: boolean;
  public multiCollapsed1: boolean;
  public multiCollapsed2: boolean;

  resultData: [];
  term: string;
  p: number =1;
  profilename : any;
  list : [];
  length = 0;
  category : [];
  // categoryname : [];
  // gradename : [];
  // boardname : [];
  // subjectname : [];
  // languagename : [];
  firstName : [];
  // firstName : [];
  lastName : [];
  profileName : [];
  schoolName : [];
  email : [];
  phoneNumber : [];
  teacherID : [];
  groupName : [];
  address1 : [];
  address2 : [];
  area : [];
  city : [];
  country : [];
  showModal : boolean;
  fileName= 'AudioList.xlsx';
  constructor(public toastr: ToastrManager,private categoryservice: RestapiService) {

  }

  onClick(event,user)
  {
    console.log('haimodal',user)
    this.showModal = true; // Show-Hide Modal Check
    this.firstName = user.teacherID.firstName;
    this.lastName = user.teacherID.lastName;
    this.profileName = user.teacherID.profileName;
    this.schoolName = user.teacherID.schoolName;
    this.email = user.teacherID.email;
    this.phoneNumber = user.teacherID.phoneNumber;
    this.teacherID = user.teacherID.teacherID;
    this.groupName = user.teacherID.groupName;
    this.address1 = user.teacherID.address1;
    this.address2 = user.teacherID.address2;
    this.area = user.teacherID.area;
    this.city = user.teacherID.city;
    this.country = user.teacherID.country;
    
    console.log('haianothermodal',user)
     
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }

  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'AudioList');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
  ngOnInit() {
    this.isCollapsed = true;
    this.multiCollapsed1 = true;
    this.multiCollapsed2 = true;

    this.listAudio();
  }


  listAudio() {
    this.categoryservice.audioList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('audio list', this.resultData);
         this.length = this.resultData.length;
      //   for(var i=0; i<result.data.length; i++){
      //     this.categoryname = result.data[i].categoryID.categoryName;
      //     this.gradename = result.data[i].gradeID.gradeName;
      //     this.boardname = result.data[i].boardID.boardName;
      //     this.languagename = result.data[i].languageID.languageName;
      //  this.subjectname = result.data[i].subjectID.subjectName;
      //  console.log('category audio',result.data[i].categoryID.categoryName)
      //  }
       
       } else {
        console.log('No Data');
      }
       });
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  //listAudio() {
   // this.categoryservice.detaillist().subscribe((result) => {
     // if (result) {
       //  this.resultData =  result.data;
         
        // console.log('audio list', this.resultData);
       // for(var i=0; i<result.data.length; i++){
         // this.categoryname = result.data[i].categoryID.categoryName;
        //  this.gradename = result.data[i].gradeID.gradeName;
         // this.boardname = result.data[i].boardID.boardName;
         // this.languagename = result.data[i].languageID.languageName;
         // this.subjectname = result.data[i].subjectID.subjectName;
          // console.log('category audio',result.data[i].categoryID.categoryName)
       //// }
       // } else {
       //  //console.log('No Data');
       // }
  //    // });
 // }
}
