import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormElementsRoutingModule } from './form-elements-routing.module';
import {SharedModule} from '../../../theme/shared/shared.module';
//import { UiBasicRoutingModule } from '../../ui-elements/ui-basic/ui-basic-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormElementsRoutingModule,
    SharedModule,
   // UiBasicRoutingModule
  ],
  declarations: []
})
export class FormElementsModule { }
