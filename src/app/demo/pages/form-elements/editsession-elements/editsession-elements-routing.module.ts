import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditSessionElementsComponent} from './editsession-elements.component';

const routes: Routes = [
  {
    path: '',
    component: EditSessionElementsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditSessionElementsRoutingModule { }