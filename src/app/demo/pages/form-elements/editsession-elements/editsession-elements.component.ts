import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service'
import { ToastrManager } from 'ng6-toastr-notifications';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, ValidatorFn } from '@angular/forms';
@Component({
  selector: 'app-basic-elements',
  templateUrl: './editsession-elements.component.html',
  styleUrls: ['./editsession-elements.component.scss']
})
export class EditSessionElementsComponent implements OnInit {
  meetingForm: FormGroup;
  
  submitted = false;
  selectedValue = "";
  formData;

  instructorapi: any = [];
teacherid="";
  resultData: [];
  languageapi: [];
  categoryapi: [];
  gradeapi: [];
  boardapi: [];
  subjectapi: [];
  public id:any = '';
  public  parentMeetingID:any = '';
  constructor(private categoryservice: RestapiService ,public location: Location, public toastr: ToastrManager,private formBuilder: FormBuilder) { }

  ngOnInit() {
   this.listinstructor();
                 
     this.meetingForm = this.formBuilder.group({

   
        topic: [''],
         time: [''],
        
            lession: [''],
            instructor:[''],
                 scheduledate:[''],
                 description:['']
     })
 var urlArray = window.location.href.split(':');
   
     this.id=urlArray[urlArray.length-1];
    

    this.getlistDetail(this.id);

  }
 selectChange (event:any) {
   var id = event.target.value;
var splitdata=event.target.value.split(":");
var email=splitdata[1];
console.log(email);
var rst={
  email:email,
}
   this.categoryservice.gettecherid(rst).subscribe((result)=>{
    if(result){
this.teacherid=result.data[0]._id;
console.log(this.teacherid);
   }else{

    }
   })
//this.instructorapi.find(instructor => instructor.email === selectedName);
   //var getid=this.getSelectedProductByName(email[1]);
   //this.selectedValue = event.target.options[event.target.selectedIndex].text;
  }
  getchange(e){
     var id = e.getAttribute('data-id');
    console.log(id);
  }
  listCategory() {
    this.categoryservice.categoryList().subscribe((result) => {
      if (result) {
         this.categoryapi =  result.data;
         console.log('user list', this.categoryapi);
       
        
      }
        else {
         console.log('No Data');
        }
       });
  }

  listboard() {
    this.categoryservice.boardList().subscribe((result) => {
      if (result) {
         this.boardapi =  result.data;
         console.log('user list', this.boardapi);
        } else {
         console.log('No Data');
        }
       });
  }

  listgrade() {
    this.categoryservice.gradeList().subscribe((result) => {
      if (result) {
         this.gradeapi =  result.data;
         console.log('user list', this.gradeapi);
        } else {
         console.log('No Data');
        }
       });
  }

  listsubject() {
    this.categoryservice.subjectList().subscribe((result) => {
      if (result) {
         this.subjectapi =  result.data;
         console.log('user list', this.subjectapi);
        } else {
         console.log('No Data');
        }
       });
  }
 listinstructor() {
    this.categoryservice.listinstructor().subscribe((result) => {
      if (result) {
         this.instructorapi =  result.data;
         
      //    if(Object.keys(this.paramData).length == 0){
      //   this.meetingForm.patchValue({ instructor: this.instructorapi[0]._id })
      // }
      console.log(this.instructorapi);
      }
        else {
         console.log('No Data');
        }
       });
  }
  listlanguage() {
    this.categoryservice.languageList().subscribe((result) => {
      if (result) {
         this.languageapi =  result.data;
         console.log('user list', this.languageapi);
        } else {
         console.log('No Data');
        }
       });
  }
  get f() { return this.meetingForm.controls; }
   backto(){
    this.location.back();
  }
getlistDetail(id){
console.log(this.id,"iddddd");
  this.categoryservice.getlistdetail({id:this.id}).subscribe((response) => {
  if(response.status){
         
              console.log(response.data);
            var editData=response.data;
             console.log(editData);
             this.parentMeetingID= editData[0].parentMeetingID;
                  this.meetingForm.patchValue({ instructor: editData[0].instructor.profileName+"     :"+editData[0].instructor.email})
                  this.teacherid=editData[0].instructor._id;
                  this.meetingForm.patchValue({ topic:  editData[0].topic})
                   this.meetingForm.patchValue({ time:  editData[0].time})
                    this.meetingForm.patchValue({ lession:  editData[0].lession})
                     this.meetingForm.patchValue({ scheduledate:  editData[0].ScheduleDate})
                    
this.meetingForm.patchValue({ description:  editData[0].description})
      
      
              }
               else {
          this.toastr.errorToastr(response.message);
        }
            })      
          
       }
  
  onSubmit() {
       this.submitted = true;

    
    var data = this.meetingForm.value;
    console.log("data",data);
   
   this.formData = {
     instructor: this.teacherid,
          lession:data.lession,
        ScheduleDate:data.scheduledate,
          time:data.time,
          topic:data.topic,
          
          id:this.id,
          description:data.description,

parentMeetingID:this.parentMeetingID

   };
  console.log("frm",this.formData);


          this.categoryservice.editsession(this.formData).subscribe((response) => {
            if(response.status){
                this.toastr.successToastr('Hooray! Your data has been updated');
                    this.backto()
                 }else {
               this.toastr.errorToastr(response.message);
              }
            })      
          
       
  }

}
  