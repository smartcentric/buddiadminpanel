import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSessionElementsComponent } from './editsession-elements.component';

describe('EditSessionElementsComponent', () => {
  let component: EditSessionElementsComponent;
  let fixture: ComponentFixture<EditSessionElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSessionElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSessionElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
