import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditSessionElementsRoutingModule } from './editsession-elements-routing.module';
import { EditSessionElementsComponent } from './editsession-elements.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    EditSessionElementsRoutingModule,
    SharedModule,
    NgbDropdownModule
  ],
  declarations: [EditSessionElementsComponent]
  })
export class EditSessionElementsModule { }
