import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../../restapi.service'
import { ToastrManager } from 'ng6-toastr-notifications';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators, ValidatorFn } from '@angular/forms';
@Component({
  selector: 'app-basic-elements',
  templateUrl: './basic-elements.component.html',
  styleUrls: ['./basic-elements.component.scss']
})
export class BasicElementsComponent implements OnInit {
  meetingForm: FormGroup;


  submitted = false;
  selectedValue = "";
  formData;
  resultData: [];
  languageapi: any =[];
  categoryapi: any = [];
  instructorapi: any = [];
teacherid="";
  gradeapi: any = [];
  boardapi: any = [];
  subjectapi: any = [];
  public paramData:any = [];
  constructor(private categoryservice: RestapiService ,public location: Location, public toastr: ToastrManager,private formBuilder: FormBuilder) { }

  ngOnInit() {
     this.listCategory();
    this.listboard();
    this.listlanguage();
    this.listinstructor();

    // this.instructorapi();

    this.listgrade();
    this.listsubject();
    this.listinstructor();
    var profileName = localStorage.getItem('profileName');
     profileName = profileName.replace(/ /gi, "+");
    var meetingId = profileName+"-"+Math.round(Math.random() * 1000000000);
     this.meetingForm = this.formBuilder.group({

     classid: [meetingId,Validators.required],
     classname: ['',Validators.required],
      exam: ['',Validators.required],
       session: ['',Validators.required],
      
        
          
             enroll: ['',Validators.required],
               meetingtype: [''],
                subject: [''],
                 board: [''],
                 grade:[''],
                 category:[''],
                 language:[''],
                 enddate:['',Validators.required],
                 startdate:['',Validators.required],
                 instructor:['']
     })
    
  }
 selectChange (event:any) {
   var id = event.target.value;
var splitdata=event.target.value.split(":");
var email=splitdata[1];
console.log(email);
var rst={
  email:email,
}
   this.categoryservice.gettecherid(rst).subscribe((result)=>{
    if(result){
this.teacherid=result.data[0]._id;
console.log(this.teacherid);
   }else{

    }
   })
//this.instructorapi.find(instructor => instructor.email === selectedName);
   //var getid=this.getSelectedProductByName(email[1]);
   //this.selectedValue = event.target.options[event.target.selectedIndex].text;
  }
  getchange(e){
     var id = e.getAttribute('data-id');
    console.log(id);
  }
  listCategory() {
    this.categoryservice.categoryList().subscribe((result) => {
      if (result) {
         this.categoryapi =  result.data;
         
         if(Object.keys(this.paramData).length == 0){
        this.meetingForm.patchValue({ category: this.categoryapi[0]._id })
      }
      console.log(this.categoryapi);
      }
        else {
         console.log('No Data');
        }
       });
  }
  listinstructor() {
    this.categoryservice.listinstructor().subscribe((result) => {
      if (result) {
         this.instructorapi =  result.data;
         
      //    if(Object.keys(this.paramData).length == 0){
      //   this.meetingForm.patchValue({ instructor: this.instructorapi[0]._id })
      // }
      console.log(this.instructorapi);
      }
        else {
         console.log('No Data');
        }
       });
  }
onProductChanged(instructor) {
    console.log(instructor);
    this.meetingForm.patchValue({ instructor: this.getSelectedProductByName(instructor) })
    
}

 getSelectedProductByName(selectedName: string) {
    return this.instructorapi.find(instructor => instructor.email === selectedName);
}
  listboard() {
    this.categoryservice.boardList().subscribe((result) => {
      if (result) {
         this.boardapi =  result.data;
         console.log('user list', this.boardapi);
          if(Object.keys(this.paramData).length == 0){
        this.meetingForm.patchValue({ board: this.boardapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }


  listgrade() {
    this.categoryservice.gradeList().subscribe((result) => {
      if (result) {
         this.gradeapi =  result.data;
         console.log('user list', this.gradeapi);
         if(Object.keys(this.paramData).length == 0){
        this.meetingForm.patchValue({ grade: this.gradeapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  listsubject() {
    this.categoryservice.subjectList().subscribe((result) => {
      if (result) {
         this.subjectapi =  result.data;
         console.log('user list', this.subjectapi);
          if(Object.keys(this.paramData).length == 0){
        this.meetingForm.patchValue({ subject: this.subjectapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }

  listlanguage() {
    this.categoryservice.languageList().subscribe((result) => {
      if (result) {
         this.languageapi =  result.data;
         console.log('user list', this.languageapi);
            if(Object.keys(this.paramData).length == 0){
        this.meetingForm.patchValue({ language: this.languageapi[0]._id })
      }
        } else {
         console.log('No Data');
        }
       });
  }
  get f() { return this.meetingForm.controls; }
   backto(){
    this.location.back();
  }

  onSubmit() {
      var btn = (document.getElementById('submitButton') as HTMLButtonElement);
      btn.disabled = true;
       this.submitted = true;
       if (this.meetingForm.invalid) {
        btn.disabled = false;
      return;
    }
    
    var data = this.meetingForm.value;
    console.log("data",data);
   
   this.formData = {
        meetingID: data.classid,
        instructor: this.teacherid,
        name: data.classname,
        startTime: data.startdate ,
        endTime: data.enddate,
        languageID: data.language,
        categotyID: data.category,
         gradeID: data.grade,
          boardID: data.board,
          subjectID: data.subject,
          meetingType: data.meetingtype,
          maxentrolledstudent:data.enroll,
        
          numberofsession:data.session,
          numberofexam: data.exam,





   };
  console.log("frm",this.formData);
  this.categoryservice.savedetail(this.formData).subscribe((response) => {
console.log(response);
  if(response.status){
  this.formData.parentMeetingID = response.data._id;
          this.categoryservice.savesession(this.formData).subscribe((response) => {
            if(response.status){
                this.toastr.successToastr('Hooray! Your data has been created');
                    this.backto()
                 }else {
               this.toastr.errorToastr(response.message);
              }
            })      
          
        }else {
          btn.disabled = false;
          this.toastr.errorToastr(response.message);
        }
  })
  }

}
  