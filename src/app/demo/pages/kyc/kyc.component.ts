import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.scss']
})
export class KycComponent implements OnInit {
  public kycdetails:any = [];
  public teacherID:any = '';
  public kycStatusReject:Boolean = false;
  public isAccept:Boolean = false;
  public TeacherDetails:any = [];
  constructor(private location:Location,private router:Router,public toastr: ToastrManager,private categoryservice: RestapiService,private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    var urlArray = window.location.href.split('/');
   
    this.teacherID=urlArray[urlArray.length-1];
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.getTeacherKYC({teacherID:this.teacherID},webToken).subscribe((result) => {
      console.log(result)
      if(result.status && result.data.data){
        this.kycdetails = result.data.data;
      }
      if(result.status && result.data.kycStatus){
        this.kycStatusReject = result.data.kycStatus.rejected
        this.isAccept = result.data.kycStatus.accepted;
      }
      if(result.status && result.data.teacherDetails){
        this.TeacherDetails = result.data.teacherDetails;
      }
    });
  }
  redirectImage(image){
    window.open(image)
  }
  kycStatusUpdate(isAccept){
    const webToken = localStorage.getItem('webtoken');
    this.categoryservice.updateKyc({teacherID:this.teacherID,isApproved:isAccept},webToken).subscribe((result) => {
      if(result.status){
        this.location.back();
        this.toastr.successToastr(result.message)
      }else{
        this.toastr.errorToastr(result.message)
      }
     
    });
  }

}
