import { Component, OnInit } from '@angular/core';
import { RestapiService } from '../../../../../restapi.service';
  import { from } from 'rxjs';
  import * as XLSX from 'xlsx'; 
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-tbl-basic',
  templateUrl: './tbl-basic.component.html',
  styleUrls: ['./tbl-basic.component.scss']
})
export class TblBasicComponent implements OnInit {
 resultData: [];
 term: string;
 p: number =1;
 profilename : any;
  length = 0;
  fileName= 'SchoolList.xlsx';
  constructor(private userService: RestapiService,private router:Router) { }
 

 exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
    
  ngOnInit() {
    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 5,
    //   processing: true
    // };

    this.listUser();
  }
  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  listUser() {
    this.userService.studentList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('school list', this.resultData)
         this.length = this.resultData.length;
        } else {
         console.log('No Data');
        }
       });
  }
  goto(id){
    this.router.navigate(['/tbl-bootstrap/freeClass/'+id]);
  }


 
}
