import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { RestapiService } from '../../../../../restapi.service';
import { Location }from '@angular/common';
@Component({
  selector: 'app-free-class',
  templateUrl: './free-class.component.html',
  styleUrls: ['./free-class.component.scss']
})
export class FreeClassComponent implements OnInit {
  public classCount = 0;
  public StudentID = '';
  public packageDetails:any = '';
  public disableButton:Boolean = false;
  public sum:Number = 0;
  public freeClassDetails:any = [];
  constructor(private location: Location,private router:Router, private activeRoute:ActivatedRoute,public toastr: ToastrManager,private userApi:RestapiService) {}

  ngOnInit(): void {
    this.StudentID = this.activeRoute.snapshot.paramMap.get('id');
    this.isalreadyAdded();
  }
  isalreadyAdded(){
    var data = {
      userId:this.StudentID 
    }
    const webToken = localStorage.getItem('webtoken');
    this.userApi.isFreeClass(webToken,data).subscribe((result) => {
      if(result.data.isfree==null){
        this.disableButton = false;
        this.packageDetails = result.data.freeProduct._id;        
      
      }else{
        this.freeClassDetails= result.data.isfree;
        this.classCount = this.freeClassDetails.count;
        this.disableButton = true
        this.toastr.errorToastr('Already Free Class added');
      }
      this.sum = result.data.allFreeClass
    });
  }
  addFreeClass(){
    const webToken = localStorage.getItem('webtoken');
    var req=
      {
        _id : this.packageDetails,
        productPrice  : 0,
        remainingClass  : this.classCount,
        remainingExam : 0,
        count   : this.classCount,
        isFree : true,
        userId : this.StudentID
      }
    
    this.userApi.addfreeClass(webToken,req).subscribe((response) => {
      if(response.status){
        this.toastr.successToastr('Your One-on-one '+this.classCount+' free Class added Successfully');
      }else{
        this.toastr.errorToastr(response.message);
      }
      this.location.back()
    }); 
  }
  updateFreeClass(isUpdate){
    var update = 0;
    var countLast = 0;
    if(isUpdate){
      update  =this.classCount - (this.freeClassDetails.count -this.freeClassDetails.remainingClass) 
      countLast = this.classCount;
    }else{
      if(this.freeClassDetails.length==0){
        this.addFreeClass()
      }else{
        update = this.freeClassDetails.remainingClass  + this.classCount;
        countLast = this.freeClassDetails.count + this.classCount
      }     
    }
   
  var req={
    userId  : this.StudentID,
    remainingClass  :update,
    count : countLast
  }
  const webToken = localStorage.getItem('webtoken');
  if(this.freeClassDetails.length!=0){
  this.userApi.updateClass(webToken,req).subscribe((res)=>{
    if(res.status){
      var text = isUpdate ? 'update':'added'
      this.toastr.successToastr('Your One-on-one '+this.classCount+' free Class '+text+' Successfully');
    }else{
      this.toastr.errorToastr(res.message);
    }
    this.location.back()
  })
  }
 
}

}
