import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FreeClassRoutingModule } from './free-class-routing.module';
import { FreeClassComponent } from './free-class.component';
import {SharedModule} from '../../../../../theme/shared/shared.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
@NgModule({
  declarations: [FreeClassComponent],
  imports: [
    CommonModule,
    FreeClassRoutingModule,
    SharedModule,
    NgxPaginationModule,
    
    Ng2SearchPipeModule,
    OrderModule
  ]
})
export class FreeClassModule { }
