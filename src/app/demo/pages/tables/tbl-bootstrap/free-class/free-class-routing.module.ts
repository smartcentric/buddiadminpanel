import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FreeClassComponent} from './free-class.component';

const routes: Routes = [
  {
    path: '',
    component: FreeClassComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FreeClassRoutingModule { }
