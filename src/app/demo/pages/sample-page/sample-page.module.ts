import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SamplePageRoutingModule } from './sample-page-routing.module';
import { SamplePageComponent } from './sample-page.component';
import {SharedModule} from '../../../theme/shared/shared.module';

import {DataTablesModule} from 'angular-datatables';

import { NgxPaginationModule } from 'ngx-pagination';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderModule } from 'ngx-order-pipe';
import { KycComponent } from '../kyc/kyc.component'
@NgModule({
  declarations: [SamplePageComponent,KycComponent],
  imports: [
    CommonModule,
    SamplePageRoutingModule,
    SharedModule,
    Ng2SearchPipeModule,
    DataTablesModule,
    NgxPaginationModule,
    OrderModule
    
  ]
})
export class SamplePageModule { }
