import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SamplePageComponent} from './sample-page.component';
import { KycComponent } from '../kyc/kyc.component'
const routes: Routes = [
  {
    path: '',
    component: SamplePageComponent
  },
  {
    path: 'kyc/:id',
    component:KycComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SamplePageRoutingModule { }
