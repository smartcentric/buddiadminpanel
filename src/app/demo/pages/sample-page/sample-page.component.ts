import { Component, OnInit } from '@angular/core';
import {RestapiService} from '../../../restapi.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import * as XLSX from 'xlsx'; 
@Component({
  selector: 'app-sample-page',
  templateUrl: './sample-page.component.html',
  styleUrls: ['./sample-page.component.scss']
})
export class SamplePageComponent implements OnInit {
  resultData:any= [];
  resultDataExport:any= [];
  term: string;
  categories: [];
  categoryname : [];
  gradename : [];
  boardname : [];
  subjectname : [];
  languagename : [];
  length = 0;
  p: number =1;
  profilename : any;
  registerForm: FormGroup;
  selectedFileNmae: string = 'Upload Photo';
  fileName= 'TeacherList.xlsx';
  dtOptions: DataTables.Settings = {};
  constructor(private router:Router,public toastr: ToastrManager,private categoryservice: RestapiService,private formBuilder: FormBuilder) { }

//   export() {
//     console.log("Export");
//     this.resultDataExport = [];
//     //this.resultDataExport = this.resultData;

//     for (let i = 0; i < this.resultData.length; i++) {
//       // this.resultDataExport.push({profileName: `profileName${i}`, lastName: `firstName${i}`,
//       // email: this.resultDataExport[i].profileName, address: `lastName${i} street city, ST`, zipcode: `gender${i}`});
      
//        this.resultDataExport.push({'Profile Name': this.resultData[i].profileName, lastName: this.resultData[i].lastName,
//        email: this.resultData[i].email, address: this.resultData[i].address,zipcode: this.resultData[i].zipcode});

//       //console.log(this.resultData[i].profileName);
//     }
// console.log(this.resultDataExport);
//     this.categoryservice.exportJsonAsExcelFile(this.resultDataExport, 'customers');
//   }
  exportexcel(): void 
    {
       /* table id is passed over here */   
       let element = document.getElementById('excel-tableHide'); 
       const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

       /* generate workbook and add the worksheet */
       const wb: XLSX.WorkBook = XLSX.utils.book_new();
       XLSX.utils.book_append_sheet(wb, ws, 'TeacherList');

       /* save to file */
       XLSX.writeFile(wb, this.fileName);
			
    }
    
  ngOnInit() {

    this.listCategory();

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true
    };

    this.registerForm = this.formBuilder.group({
      categoryName: ['', Validators.required],
      image: ['', Validators.required],
  });

  this.categoryservice.teaccherList().subscribe((result) => {
    if (result) {
       this.resultData =  result.data;
       this.resultDataExport = result
       console.log('user list', this.resultData);
       this.length = this.resultData.length;
   
      } else {
       console.log('No Data');
      }
     });
    
  console.log("After par");
  console.log(this.resultData);
    
  }

  key: string = 'profilename';
  reverse : boolean =false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  fileChange(event) {
    let fileList: FileList = event.target.files;

    this.registerForm.patchValue({ upload_name: fileList[0] });
    this.selectedFileNmae = fileList[0].name;
    this.registerForm.patchValue({ image: this.selectedFileNmae });
    console.log('upload photi',this.selectedFileNmae)

  }

  get f() { return this.registerForm.controls; }
  goto(id){
    this.router.navigate(['/sample-page/kyc/'+id]);
  }
  onSubmit(){
  if(this.registerForm.valid){
    const data = this.registerForm.value;
    this.categoryservice.addmeeting(data).subscribe(res => {
      if(res.status){
        console.log('success')
      }
      else{
        console.log('failure')
      }
    });
  }
  }

  listCategory() {
    this.categoryservice.teaccherList().subscribe((result) => {
      if (result) {
         this.resultData =  result.data;
         console.log('user list', this.resultData);
         this.length = this.resultData.length;
     
        } else {
         console.log('No Data');
        }
       });
  }

  deleteRemainder(data){
    if(confirm("Are you sure to delete?")) {
      console.log(data);
      var passJson = data._id
      console.log('sa',passJson)
      this.categoryservice.deleteCategory(passJson).subscribe((result) => {
       if (result.status) {
          this.toastr.errorToastr('Categorys deleted','Success',{
             position:'top-right'
          });
          this.ngOnInit();
        }
       }, (err) => {
       console.log(err);
       });
    }
   }

}
