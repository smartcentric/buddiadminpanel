export interface AuthData {
   firstName: string;
   profileName: string;
   email: string;
   passWord: string;
   userType: number;
 }
