import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthserviceService } from '../authservice.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-auth-signin',
  templateUrl: './auth-signin.component.html',
  styleUrls: ['./auth-signin.component.scss']
})
export class AuthSigninComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthserviceService,
              private toastr: ToastrManager
              ) { }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
              email: [null, [Validators.required, Validators.email]],
              password: [null, Validators.required]
              });

  }
  get fval() { return this.loginForm.controls; }

  onFormSubmit() {
        this.submitted = true;
        if (this.loginForm.invalid) {
          return;
        }
        this.loading = true;
        const loginData =  {} as any;
        loginData.email = this.fval.email.value;
        loginData.password = this.fval.password.value;
        this.authenticationService.login(loginData).subscribe((data) => {
          this.loading = false;
          if (data.success) {
            localStorage.setItem('webtoken',data.token);
             localStorage.setItem('profileName', data.name);
             localStorage.setItem('admintype', data.adminID);
            console.log('admin',data.adminID)
            console.log('token',data.token)
            this.router.navigate(['dashboard/analytics']);
            this.toastr.successToastr('Logged in Successfully', 'Success');
          } else {
            this.toastr.errorToastr(data.message, 'Error');
          }
        });
      }
}
