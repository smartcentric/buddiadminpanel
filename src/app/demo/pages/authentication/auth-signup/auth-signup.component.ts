import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthserviceService } from '../authservice.service';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth-signup.component.html',
  styleUrls: ['./auth-signup.component.scss']
})
export class AuthSignupComponent implements OnInit {

  signupForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthserviceService,
              private toastr: ToastrManager
    ) { }


  ngOnInit() {

    this.signupForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      profileName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      passWord: ['', Validators.required],
      userType: ['', Validators.required]
      });

  }

  get formval() {
    return this.signupForm.controls;
  }

  onSave() {
    this.submitted = true;
    if (this.signupForm.invalid) {
          return;
        }

    this.loading = true;
    const signupData =  {} as any;
    signupData.first_name = this.formval.firstName.value;
    signupData.profileName = this.formval.profileName.value;
    signupData.email = this.formval.email.value;
    signupData.passWord = this.formval.passWord.value;
    signupData.userType = this.formval.userType.value;
    this.authenticationService.createUser(signupData).subscribe((data) => {
    this.loading = false;
    if (data.success) {
            this.toastr.successToastr('User Registered Successfully', 'Success');
            this.router.navigate(['signin']);
          } else {
            return this.toastr.errorToastr(data.message, 'Error');
           // return this.toastr.errorToastr('User Registration Failed', 'Error');
          }
        });
      }

  }
