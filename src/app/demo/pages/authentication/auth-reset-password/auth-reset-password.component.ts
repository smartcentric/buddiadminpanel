import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AuthData  } from 'src/app/demo/pages/authentication/auth-data.model';
import { from } from 'rxjs';
import { AuthserviceService } from '../authservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrManager } from 'ng6-toastr-notifications';


@Component({
  selector: 'app-auth-reset-password',
  templateUrl: './auth-reset-password.component.html',
  styleUrls: ['./auth-reset-password.component.scss']
})
export class AuthResetPasswordComponent implements OnInit {

  PassResetForm: FormGroup;
  loading = false;
  submitted = false;

  constructor( private formBuilder: FormBuilder,
               private route: ActivatedRoute,
               private router: Router,
               private toastr: ToastrManager,
               private authenticationService: AuthserviceService) { }

  ngOnInit() {

    this.PassResetForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]]

      });
  }

  get resetfval() { return this.PassResetForm.controls; }


  onPassReset() {
    this.submitted = true;
    if (this.PassResetForm.invalid) {
      return;
    }
    this.loading = true;
    const passResetData =  {} as any;
    passResetData.email = this.resetfval.email.value;
    this.authenticationService.forgotPage(passResetData).subscribe((data) => {
      this.loading = false;
      if (data.status === true) {
        this.router.navigate(['signin']);
        this.toastr.successToastr('Reset Password Email Sent Successfully', 'Success');
      } else {
        this.toastr.errorToastr(data.message, 'Error');
      }
    });
  }
}
