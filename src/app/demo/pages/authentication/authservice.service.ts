import { Injectable } from '@angular/core';
import { RestapiService } from '../../../restapi.service';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class AuthserviceService {
  constructor(private api: RestapiService) { }
  login(param: any) {
    return this.api.post('admin/login', param);
  }

  createUser(param: any) {
    return this.api.post('user/userCreate', param);
   }

  forgotPage(param: any) {
     return this.api.post('user/users/forgot', param);
   }



}
